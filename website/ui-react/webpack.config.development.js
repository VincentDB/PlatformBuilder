const _ = require('lodash')
const webpack = require('webpack')
const utils = require('./webpack.config.utils')
const baseConfig = require('./webpack.config.base')
const path = require('path')

let backend = process.env.BACKEND
if (['dummy', 'firebase'].indexOf(backend) < 0) {
  throw new Error('Unknown backend: ' + backend)
}

module.exports = Object.assign({}, baseConfig, {
  devServer: {
    contentBase: path.resolve(__dirname, 'src'),
    port: 9000
  },
  entry: utils.createEntry(true),
  devtool: 'source-map',
  plugins: _.concat([
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'BACKEND': JSON.stringify(backend),
      'TIER': JSON.stringify('dev')
    }),
  ], baseConfig.plugins)
})
