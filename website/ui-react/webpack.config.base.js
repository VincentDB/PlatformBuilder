// var webpack = require('webpack')
const fs = require('fs')
const path = require('path')

module.exports = {
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    modules: [
      path.join(__dirname, 'src', 'js'),
      'node_modules',
    ],
    alias: {
      'ui-common': path.resolve(__dirname, '../ui-common'),
      // 'ui-tools': path.resolve(__dirname, '../ui-common'),
    }
  },
  output: {
    filename: 'bundle.js',
    publicPath: ''
  },
  devtool: 'source-map',
  plugins: [
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        include: [
          path.join(__dirname, 'src', 'js'),
          path.join(__dirname, 'test')
        ],
        exclude: path.join(__dirname, 'node_modules'),
      },
      {
        test: /\.tsx?$/,
        use: 'awesome-typescript-loader',
        include: [
          path.join(__dirname, 'src', 'js'),
          path.resolve(__dirname, '../ui-common/ts'),
          fs.realpathSync(path.resolve(__dirname, 'node_modules/rapid-ui/ts')),
        ],
      },
      {
        test: /\.html$/,
        use: 'file-loader?name=[name].[ext]'
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      }
    ]
  }
}
