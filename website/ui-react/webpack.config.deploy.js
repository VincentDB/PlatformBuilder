const _ = require('lodash')
const path = require('path')
const webpack = require('webpack')
const utils = require('./webpack.config.utils')
const baseConfig = require('./webpack.config.base')

const tier = process.env.TIER
if (['staging', 'production'].indexOf(tier) < 0) {
  throw new Error(
    'Got invalid TIER env variable. Must be either production or staging, was ' + tier
  )
}

let backend = process.env.BACKEND
if (!backend) {
  backend = 'firebase'
}
if (['dummy', 'firebase'].indexOf(backend) < 0) {
  throw new Error('Unknown backend: ' + backend)
}
if (tier === 'production' && backend !== 'firebase') {
  throw new Error('Cannot deploy to production without Firebase backend, got ' + backend)
}

module.exports = Object.assign({}, baseConfig, {
  entry: utils.createEntry(false),
  output: Object.assign({}, baseConfig.output, {
    path: path.join(__dirname, 'dist/' + tier),
  }),
  plugins: _.concat([
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.DefinePlugin({
      'BACKEND': JSON.stringify(backend),
      'TIER': JSON.stringify(tier)
    }),
  ], baseConfig.plugins)
})
