var fs = require('fs')
var path = require('path')
var yaml = require('js-yaml');

module.exports = function (source) {
  // this.cacheable && this.cacheable()

  // console.log('LOADER TRIGGERED....')

  var res = yaml.safeLoad(source, {
    filename: this.resourcePath
  })
  if (res.index) {
    const rootPath = path.resolve('../ui-data/state')
    const modules = {}
    fs.readdirSync(rootPath).forEach(moduleFileName => {
      if (path.extname(moduleFileName) !== '.yaml' || moduleFileName === 'index.yaml') {
        return
      }

      const modulePath = path.resolve(rootPath, moduleFileName)
      this.addDependency(modulePath)

      const moduleName = moduleFileName.split('.').slice(0, -1).join('.')
      const module = yaml.safeLoad(fs.readFileSync(modulePath), {
        filename: this.resourcePath
      })
      modules[moduleName] = module
    })
    res = modules
  }
  // process(res, this.addDependency)

  // console.log(JSON.stringify(res, null, 2))

  return JSON.stringify(res, null, '\t')
}
