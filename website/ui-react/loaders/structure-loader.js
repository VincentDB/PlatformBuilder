var fs = require('fs')
var path = require('path')
var yaml = require('js-yaml')

const process = (definition, addDependency) => {
  if (definition.type.indexOf('include') === 0) {
    definition.type = definition.type.split(' ')[1]
    const [module, name] = definition.type.split(':')
    const includePath = path.resolve('../ui-data/structure', module, `${name}.yaml`)
    
    addDependency(includePath)
    const include = yaml.safeLoad(fs.readFileSync(includePath), {
      filename: this.resourcePath
    })
    Object.assign(definition, include)
  }
  if (definition.children instanceof Array) {
    for (const child of definition.children) {
      // console.log('child of', definition.type, child.type)
      process(child, addDependency)
    }
  }
  if (definition.content instanceof Array) {
    for (const child of definition.content) {
      process(child, addDependency)
    }
  }
}

module.exports = function (source) {
  // this.cacheable && this.cacheable()

  // console.log('LOADER TRIGGERED....')

  var res = yaml.safeLoad(source, {
    filename: this.resourcePath
  })
  process(res, this.addDependency)

  // console.log(JSON.stringify(res, null, 2))

  return JSON.stringify(res, null, '\t')
}
