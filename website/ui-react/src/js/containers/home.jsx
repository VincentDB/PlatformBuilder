const _ = require('lodash')
import React from 'react'
import Radium from 'radium'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import * as colors from 'material-ui/colors'
import * as reduxGlobals from '../redux/globals'
import StyleRegistry from 'rapid-ui/ts/utils/style-registry'
// import LogicRegistry from 'rapid-ui/ts/utils/logic-registry'
import ComponentRegistry from 'rapid-ui/ts/utils/component-registry'
import processStructure from 'rapid-ui/ts/utils/structure-processor'
import componentModules from '../component-modules'
import StructureComponent from '../components-rapid/structure-component'

const STYLE_REGISTRY = new StyleRegistry(require('json-loader!yaml-loader!../../../../ui-data/style/app.yaml'))
// const logic = new LogicRegistry(
//   require('json-loader!yaml-loader!../../../../ui-data/rendering-logic/app.yaml')
// )
const COMPONENTS = new ComponentRegistry(componentModules)
const STRUCTURE_DEFINITION = require(
  'json-loader!../../../loaders/structure-loader!../../../../ui-data/structure/app.yaml'
)
const STRUCTURE = processStructure(STRUCTURE_DEFINITION, {COMPONENTS})
const STATE_DEFINITIONS = require('json-loader!yaml-loader!../../../../ui-data/states.yaml')

const THEME = createMuiTheme({
  palette: {
    secondary: colors.green,
    primary: colors.lightBlue
  }
})

const STYLES = {
  root: {
    // position: 'fixed',
    // top: 0,
    // left: 0,
    // width: '100%',
    // height: '100%',
    // overflow: 'auto',
    minHeight: '1000px',
    backgroundColor: THEME.palette.background.default,
  }
}

@Radium
export default class HomePage extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={THEME}>
        <div style={STYLES.root}>
          <StructureComponent
            stateDefinitions={STATE_DEFINITIONS}
            structure={STRUCTURE}
            styles={STYLE_REGISTRY}
            components={COMPONENTS}
            reduxGlobals={reduxGlobals}
            location={this.props.location}
          />
        </div>
      </MuiThemeProvider>
    )
  }
}
