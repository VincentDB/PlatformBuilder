import React from 'react'
// import { connect } from 'redux/utils'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Radium from 'radium'
import router from '../redux/modules/router'
import {
  createAction as createCompositeAction
} from 'rapid-ui/ts/redux/modules/dynamic/action-types/composite'
// import 'typeface-roboto'

@connect(
  () => ({}),
  (dispatch, props) => {
    return bindActionCreators({
      route: router.route.action,
      composite: createCompositeAction
    }, dispatch)
  }
)
@Radium
export default class State extends React.Component {
  componentDidMount() {
    const definitions = require('json-loader!yaml-loader!../../../../ui-data/states.yaml')
    const definition = definitions[this.props.params.state]
    // // console.log(this.props)
    // this.props.composite({
    //   moduleDefinitions,
    //   actionDefinition: {
    //     steps: definition.steps
    //   },
    //   input: {
    //     done: () => {
    //       this.props.route({
    //         route: definition.route.route || definition.route,
    //         params: definition.route.params || {}
    //       })
    //     }
    //   },
    //   modules
    // })

    this.props.route({
      route: definition.route.route || definition.route,
      params: definition.route.params || {},
      query: {state: this.props.params.state}
    })
  }

  render() {
    return (
      <div>Loading state</div>
    )
  }
}
