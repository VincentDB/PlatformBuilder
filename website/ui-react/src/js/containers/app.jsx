import React from 'react'
import { connect } from 'redux/utils'
import Radium, {StyleRoot} from 'radium'
// import 'typeface-roboto'

@connect({
  props: [],
  actions: []
})
@Radium
export default class App extends React.Component {
  componentDidMount() {

  }

  render() {
    return (
      <StyleRoot>
        {this.props.children}
      </StyleRoot>
    )
  }
}
