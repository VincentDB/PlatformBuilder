import React from 'react'
import Radium from 'radium'
import { connect } from 'redux/utils'

const STYLES = {
  container: {
  },
}

@connect({
  props: [],
  actions: []
})
@Radium
export default class Page extends React.Component {
  render() {
    return (
      <div style={STYLES.container}>
      </div>
    )
  }
}
