import React from 'react'

import { Router, Route, IndexRoute } from 'react-router'

import App from 'containers/app'
import Home from 'containers/home'
import State from 'containers/state'

function getRoutes() {
  return (<Route path="/" component={App} >
    <IndexRoute component={Home} />
    <Route path="state/:state" component={State} />
    <Route path="*" component={Home} />
  </Route>)
}

export default (history) => {
  return (<Router history={history}>{getRoutes()}</Router>)
}
