const _ = require('lodash')
import React from 'react'
import { withStyles } from 'material-ui/styles'
import { GridList, GridListTile } from 'material-ui/GridList'
import AppBar from 'material-ui/AppBar'
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import Paper from 'material-ui/Paper'
import Divider from 'material-ui/Divider'
import { LinearProgress } from 'material-ui/Progress'
import TextField from 'material-ui/TextField'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  // withMobileDialog,
} from 'material-ui/Dialog'

import Input, { InputLabel } from 'material-ui/Input'
import { FormControl, FormHelperText } from 'material-ui/Form'
import Select from 'material-ui/Select'

import { MenuItem } from 'material-ui/Menu'
import SimpleMenu from './simple-menu'

import MenuIcon from 'material-ui-icons/Menu'
import StarIcon from 'material-ui-icons/Star'
import StarHalfIcon from 'material-ui-icons/StarHalf'
import StarBorderIcon from 'material-ui-icons/StarBorder'

const components = {
  'app-bar': AppBar,
  'toolbar': Toolbar,
  'typography': Typography,
  'button': Button,
  'icon-button': IconButton,
  'grid-list': GridList,
  'grid-tile': GridListTile,
  'paper': Paper,
  'card': Card,
  'divider': Divider,
  'card-actions': CardActions,
  'card-content': CardContent,
  'card-media': CardMedia,
  'linear-progress': LinearProgress,
  'text-field': TextField,

  'dialog': Dialog,
  'dialog-content': DialogContent,
  'dialog-content-text': DialogContentText,
  'dialog-actions': DialogActions,
  'dialog-title': DialogTitle,

  'input': Input,
  'input-label': props => {
    return <InputLabel htmlFor={props['for']}>{props.children}</InputLabel>
  },
  'form-control': FormControl,
  'form-helper-text': FormHelperText,
  'select': props => {
    return <Select
      input={<Input id={props.id} />}
      {..._.omit(props, ['id', 'children'])}
    >
      {props.children}
    </Select>
  },

  'menu': SimpleMenu,
  'menu-item': MenuItem,

  'menu-icon': MenuIcon,
  'star-icon': StarIcon,
  'star-half-icon': StarHalfIcon,
  'star-border-icon': StarBorderIcon,
}

const styleAttributes = {
  'app-bar': {
    color: true
  },
  'icon-button': {
    color: true
  },
}

for (let componentName in components) {
  const MaterialComponent = components[componentName]
  components[componentName] = ({style, ...props}) => {
    // console.log('rendering %s (%o) with attrs %o', componentName, MaterialComponent, props)
    if (!style) {
      return React.createElement(
        MaterialComponent,
        _.omit(props, 'children'),
        props.children || null
      )
    }

    const mergedStyle = Object.assign({}, ...style)
    for (let styleAttr in styleAttributes[componentName] || {}) {
      props[styleAttr] = mergedStyle[styleAttr]
      delete mergedStyle[styleAttr]
    }

    const StyledComponent = withStyles(
      theme => {
        return { root: mergedStyle }
      }
    )(function StyledComponent(innerProps) {
      const { classes } = innerProps
      return React.createElement(
        MaterialComponent,
        {
          className: classes.root,
          ..._.omit(props, 'children')
        },
        props.children || null
      )
    })
    return <StyledComponent />
  }
}

export default components
