import React from 'react'
import Menu, { MenuItem } from 'material-ui/Menu'

class SimpleMenu extends React.Component {
  state = {
    hasRef: false
  }

  render() {
    return (
      <div style={{position: 'relative'}}>
        <div ref={(elem) => {
          this.anchorEl = elem
          if (!this.state.hasRef) {
            this.setState({hasRef: true})
          }
        }} style={{position: 'relative'}}></div>
        {this.state.hasRef && <Menu
          anchorEl={this.anchorEl}
          {...this.props}
        >
          {this.props.children}
        </Menu>}
      </div>
    )
  }
}

export default SimpleMenu
