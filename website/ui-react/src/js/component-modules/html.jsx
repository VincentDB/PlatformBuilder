import React from 'react'

export default function(componentName) {
  if (componentName === 'text') {
    return function Text(props) {
      return (<span>{props.children}</span>)
    }
  }
  return componentName
}
