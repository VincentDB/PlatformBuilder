const _ = require('lodash')
import React from 'react'
import { renderStructure } from 'rapid-ui/ts/structure'
import { DataComponent, CustomizableComponent } from './data-component'

export default function render({structure, styles, components, path}) {
  return renderStructure({
    structure,
    path,
    withKeys: true,
    renderStateNode: ({definition, initialize, render, key}) => {
      // console.log('render state node', definition.type, definition.state)
      return <DataComponent
        // styles={styles}
        definition={definition}
        render={render}
        key={key}
      />
    },
    renderNode: ({definition, generated, handlers, context, key}) => {
      // console.log('render node', definition.type, definition)

      handlers = _.mapKeys(handlers, (handler, key) => {
        return _.camelCase('on-' + key)
      })
      handlers = _.mapValues(handlers, (handler, key) => {
        return event => {
          handler(event)
        }
      })

      const style = styles.find(definition, context) || generated.styles
      const styleAttr = style ? {style} : {}
      
      let component = components.find(definition.type)
      if (!component) {
        if (handlers.onInit) {
          component = CustomizableComponent
        } else {
          component = 'div'
        }
      }

      let content = generated.children
      if (!(content && content.length)) {
        content = generated.content || null
      }

      return React.createElement(
        component,
        {
          ...generated.attrs,
          ...styleAttr,
          ...handlers,
          key
        },
        content
      )
    }
  })
}