const _ = require('lodash')
import React from 'react'
import Radium from 'radium'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import router from '../redux/modules/router'
import {
  createAction as createCompositeAction
} from 'rapid-ui/ts/redux/modules/dynamic/action-types/composite'
import renderUI from '../components-rapid/render'

@connect(
  (state) => {
    return {app: state.get('app').toJS()}
  },
  (dispatch, props) => {
    return bindActionCreators({
      route: router.route.action,
      composite: createCompositeAction
    }, dispatch)
  }
)
@Radium
export default class StructureComponent extends React.Component {
  state = {
    loaded: false
  }

  componentDidMount() {
    if (!this.props.location.query.state) {
      this.setState({loaded: true})
      return
    }

    const definitions = this.props.stateDefinitions
    const definition = definitions[this.props.location.query.state]
    // console.log(this.props)
    this.props.composite({
      moduleDefinitions: this.props.reduxGlobals.moduleDefinitions,
      actionDefinition: {
        steps: definition.steps
      },
      input: {
        done: () => {
          this.setState({loaded: true})
        }
      },
      modules: this.props.reduxGlobals.modules
    })
  }

  render() {
    if (!this.state.loaded) {
      return <div>Loading state...</div>
    }
    if (!this.props.app) {
      return null
    }

    return renderUI({
      structure: this.props.structure,
      styles: this.props.styles,
      components: this.props.components,
      path: this.props.location.pathname
    })
  }
}
