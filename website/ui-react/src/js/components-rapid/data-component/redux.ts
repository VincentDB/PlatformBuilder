const _ = require('lodash')
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as reduxGlobals from '../../redux/globals'

export default component => connect(
  (state, props) => {
    // console.log('getting state props for', props.definition.type)

    const {definition} = props
    const retrieved = {}

    const requestedState = (definition.state || [])
    requestedState.forEach(request => {
      // console.log(request)
      const [module, path] = request.split(':')
      const pathComponents = path.split('.')
      let value = state.getIn([module, ...pathComponents.map(
        component => _.camelCase(component)
      )])
      if (typeof value === 'undefined') {
        // console.log(state.toJS())
        throw new Error('No such state: ' + request)
      }
      if (value !== null && value.toJS) {
        value = value.toJS()
      }
      retrieved[_.last(pathComponents)] = value
    })

    return retrieved
  },
  (dispatch, props) => {
    const {definition} = props

    const getModuleAndActionNameFromID = (id) => {
      const [moduleName, actionName] = id.split(':')
      const module = reduxGlobals.modules[moduleName]
      return [module, _.camelCase(actionName)]
    }

    const wantedActions = props.definition.actions || []
    return bindActionCreators(_.fromPairs(wantedActions.map(id => {
      const [module, actionName] = getModuleAndActionNameFromID(id)
      let action = module.actions
        ? module.actions[actionName]
        : module[actionName]
      if (action.action) {
        action = action.action
      }
      if (typeof action === 'undefined') {
        throw new Error('No such action: ' + id)
      }
      return [actionName, action]
    })), dispatch)
  }
)(component)
