const _ = require('lodash')
import React from 'react'
import Radium from 'radium'
import reduxConnector from './redux'

@Radium
class DumbDataComponent extends React.Component {
  render() {
    const {definition} = this.props
    const state = _.fromPairs((definition.state || []).map(path => {
      const key = _.last(path.split(/\.|:/))
      return [key, this.props[key]]
    }))
    let actions = _.fromPairs((definition.actions || []).map(action => {
      const key = _.last(action.split(/\.|:/))
      const boundAction = this.props[_.camelCase(key)]
      return [key, boundAction]
    }))
    actions = _.mapValues(actions, (boundAction, key) => {
      return (params) => {
        // console.log('action %s triggered from %s with params', key, definition.type, params)
        boundAction(params)
      }
    })

    return this.props.render({state, actions})
  }
}
export const DataComponent = reduxConnector(DumbDataComponent)

export class CustomizableComponent extends React.Component {
  componentDidMount() {
    this.props.onInit()
  }

  render() {
    return <div {..._.omit(this.props, 'onInit')}>{this.props.children}</div>
  }
}
