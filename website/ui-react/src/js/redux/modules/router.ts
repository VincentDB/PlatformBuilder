import * as queryString from 'query-string'
import { push } from 'react-router-redux'
const Route = require('route-parser')

let ROUTES = {}

export function collectRoutes(structure, assign = true) {
  const routes = {}

  const { route } = structure
  if (route) {
    routes[route.name] = new Route(route.path)
  }

  for (const child of structure.children || []) {
    Object.assign(routes, collectRoutes(child, false))
  }

  ROUTES = routes

  return routes
}

export default {
  initialState: {},
  route: {
    action: ({route, params, query}) => {
      const routeObject = ROUTES[route]
      if (!routeObject) {
        throw new Error('No such route: ' + route)
      }
      let path = routeObject.reverse(params || {})
      if (query) {
        path += '?' + queryString.stringify(query)
      }
      return push(path)
    },
    reducer: state => state
  }
}
