declare var BACKEND
import _ from 'lodash'
import * as redux from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import * as routerRedux from 'react-router-redux'
import backendMiddleware from 'rapid-ui/ts/redux/middleware/backend'
import createServices from '../../../../ui-common/ts/services'
import * as reduxModules from 'rapid-ui/ts/redux/modules'
import createRootReducer from 'rapid-ui/ts/redux/reducer'
import router, {collectRoutes} from './modules/router'
import * as reduxGlobals from './globals'
// import { actions as authActions } from './modules/auth'

export default function createStore(history, client, data) {
  collectRoutes(require(
    'json-loader!../../../loaders/structure-loader!../../../../ui-data/structure/app.yaml'
  ))
  const {moduleDefinitions, modules} = createModules()
  _.assign(reduxGlobals, {moduleDefinitions, modules})

  // Sync dispatched route actions to the history
  let backend = createBackend({backendType: BACKEND})
  const services = createServices(backend, BACKEND)

  const reduxRouterMiddleware = routerRedux.routerMiddleware(history)
  const middleware = createMiddleware({backend, services, reduxRouterMiddleware})
  const createStore = getDecoratedCreateStore({middleware})
  const reducer = createRootReducer({modules, routerReducer: routerRedux.routerReducer})
  const store = createStore(reducer, data)
  setupObservers({store, services, modules, backend})
  setupHotLoader({store})

  return store
}

export function createModules() : {modules, moduleDefinitions} {
  return reduxModules.createModules({
    moduleDefinitions: require(
      'json-loader!../../../loaders/state-loader!../../../../ui-data/state/index.yaml'
    ),
    moduleOverrides: {
      router
    }
  })
}

export function createBackend({backendType}) {
  let backend
  if (backendType === 'firebase') {
    const module = require('ui-common/ts/backend/firestore')
    const Backend = module.default
    backend = new Backend(module.initFirebase().database().ref())
  } else {
    const Backend = require('ui-common/ts/backend/dummy').default
    backend = new Backend()
  }
  return backend
}

export function createMiddleware({backend, services, reduxRouterMiddleware}) {
  return [
    backendMiddleware(backend, services),
    reduxRouterMiddleware
  ]
}

export function getDecoratedCreateStore({middleware}) {
  let finalCreateStore
  if (window['__REDUX_DEVTOOLS_EXTENSION__']) {
    const { persistState } = require('redux-devtools')
    // const DevTools = require('../containers/DevTools/DevTools');
    finalCreateStore = composeWithDevTools(
      redux.applyMiddleware(...middleware),
      persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
    )(redux.createStore)
  } else {
    finalCreateStore = redux.applyMiddleware(...middleware)(redux.createStore)
  }
  return finalCreateStore
}

export function setupObservers({store, services, modules, backend}) {
  _.forEach(modules, module => {
    if (!module.observers) {
      return
    }

    _.forEach(module.observers, observer => {
      observer({store, backend, services})
    })
  })
}

export function setupHotLoader({store}) {
  if (window['__DEVELOPMENT__'] && module['hot']) {
    // module['hot'].accept('ui-common/ts/redux/reducer', () => {
    //   store.replaceReducer(require('ui-common/ts/redux/reducer').default)
    // })
  }
}
