module.exports = {
  createEntry: (devServer) => {
    let entry = [
      'babel-polyfill',
    ]

    if (devServer) {
      entry = entry.concat([
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://localhost:9000',
        'webpack/hot/only-dev-server',
      ])
    }

    entry = entry.concat([
      './src/js/main.jsx',
      './src/index.html'
    ])

    return entry
  }
}
