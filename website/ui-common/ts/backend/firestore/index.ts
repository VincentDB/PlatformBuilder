// import _ from 'lodash'
// import slugify from 'slug'
// import * as firebase from 'firebase'
// require('firebase/firestore')

export default class FirebaseBackend {
  private collectionRef

  constructor(dbRef) {
    this.collectionRef = dbRef
  }

  async reset() {
    const { parent } = this.collectionRef
    const isUnitTestCollection = parent && this.collectionRef.id == 'test-db'
    if (isUnitTestCollection) {
      await this.collectionRef.parent.delete()
    }
  }
}

export function initFirebase() {
  // var config = {
  //   apiKey: '',
  //   authDomain: '',
  //   databaseURL: '',
  //   storageBucket: '',
  //   messagingSenderId: ''
  // }
  // firebase.initializeApp(config)
  // return firebase
}
