import { processHome, processProject } from "../processors";

export default abstract class HomePageBackend {
  constructor() {}

  async retrieveFeatured() {
    const featured = await this.fetchFeatured()
    return {
      ...featured,
      homes: featured.homes.map(processHome),
      projects: featured.projects.map(processProject)
    }
  }
  abstract fetchFeatured() : Promise<any>

  abstract retrieveStats() : Promise<any>
}
