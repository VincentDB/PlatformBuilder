// const _ = require('lodash')
import { getFixtureFactory } from '../../fixtures'

export default abstract class UsersBackend {
  constructor() {}

  abstract getUserByID(id) : Promise<any>
  abstract getSemiRandomUser() : Promise<any>
}
