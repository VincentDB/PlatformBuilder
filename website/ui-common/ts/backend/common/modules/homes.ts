import { processHome, processProject } from "../processors";

export default abstract class HomesBackend {
  constructor() {}

  abstract createHome({user, home} : {user : object, home : object}) : Promise<any>
  abstract updateHome(home) : Promise<any>
  abstract findHomesByUser(user) : Promise<any>
  abstract findHomesToStay(
    {city, fromDate, toDate} :
    {city : object, fromDate? : object, toDate? : object}
  ) : Promise<any>
}
