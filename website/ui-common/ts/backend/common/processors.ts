const _ = require('lodash')
import * as moment from 'moment'
import * as numeral from 'numeral'

export function processProject(project) {
  project.roundedPercentageRaised = Math.round(project.raised / project.goal * 100)
  project.daysToDeadline = Math.ceil(moment.duration(project.deadline.valueOf() - moment().valueOf()).asDays())
  project.display = {
    raised: `€${numeral(project.raised).format('0,0[.]0')}`
  }
  return project
}

export function processHome(home) {
  home.display = {
    type: {
      home: 'Entire home',
      room: 'Entire room',
      'shared-room': 'Shared room'
    }[home.type]
  }
  home.display.typeLine = (
    `${home.display.type} - ${home.bedCount} bed${home.bedCount > 1 ? 's' : ''}`
  )

  const halvedRating = home.rating / 2.0
  const flooredRating = Math.floor(halvedRating)
  const hasHalf = halvedRating % flooredRating > 0
  const emptyStarCount = 5 - Math.ceil(halvedRating)
  home.display.stars = _.times(flooredRating, _.constant({content: 'full'}))
  if (hasHalf) {
    home.display.stars.push({content: 'half'})
  }
  home.display.stars.push(..._.times(emptyStarCount, _.constant({content: 'empty'})))
  _.each(home.display.stars, (star, idx) => {
    star.key = `star-${idx}`
  })

  return home
}
