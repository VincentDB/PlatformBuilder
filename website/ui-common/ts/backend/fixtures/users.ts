// https://github.com/peterKaleta/fixture-factory
const _ = require('lodash')
import * as faker from 'faker'
import * as fixtureFactory from 'fixture-factory'
import { FixtureFactory } from './'

const DUMMY_IMAGE_URLS = {
  'avatar_square_small': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAGFBMVEUAAAD///9/f39fX18/Pz+/v78fHx+fn59C5HjXAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQ0lEQVQokWNgGF5AGY3BopqAymBNUWBAYTgxhDGgMMKALGQGiwIDawIyA0iwFDAgMZKB2IABmUErwF5eXl5ESwvQAQC/YwqTKQG9ywAAAABJRU5ErkJggg=='
}

export default class Factory extends FixtureFactory {
  name = 'user'
  model = {
    slug: (fixtures) => faker.helpers.slugify(fixtures.displayName),
    displayName: {
      method: 'fake',
      args: ['{{name.firstName}} {{name.lastName}}']
    },
    avatarUrls: {
      squareSmall: DUMMY_IMAGE_URLS.avatar_square_small
    }
  }
  instances = 50
}
