// https://github.com/peterKaleta/fixture-factory
import * as faker from 'faker'
import * as fixtureFactory from 'fixture-factory'
import { FixtureFactory } from './'

export default class Factory extends FixtureFactory {
  name = 'home-page'
  model = {
    featured: {
      homes: [
        {object: 'ref.home'},
        3
      ],
      projects: [
        {object: 'ref.project'},
        3
      ],
      cities: [
        {object: 'ref.city'},
        3
      ],
    }
  }
  instances = [{
    stats: {
      projectsFunded: 57,
      fundsCollected: 132050,
      communityCount: 8
    }
  }]
}
