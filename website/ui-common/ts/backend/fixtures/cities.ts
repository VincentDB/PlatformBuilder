// https://github.com/peterKaleta/fixture-factory
const _ = require('lodash')
import * as faker from 'faker'
import * as fixtureFactory from 'fixture-factory'
import { FixtureFactory } from './'

export default class Factory extends FixtureFactory {
  name = 'city'
  model = {
    slug: (fixtures) => faker.helpers.slugify(fixtures.name),
    name: 'address.city'
  }
  instances = [
    {name: 'Barcelona'},
    {name: 'Venice'},
    {name: 'Amsterdam'},
    {name: 'Bologna'},
    ...(_.times(5, () => null))
  ]
}
