const _ = require('lodash')
import * as faker from 'faker'
import * as fixtureFactory from 'fixture-factory'
import { FixtureFactory } from './'

const DUMMY_IMAGE_URLS = {
  'list': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATsAAADNBAMAAAA1TspEAAAAG1BMVEUAAAD///+/v7+fn59/f38fHx8/Pz9fX1/f399DZLSnAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAEFklEQVR4nO2YS1PbMBSFDbGTLJFDGy/jTts1gc60yzBkgGUyaQvLuC0NS9IH2eLSUn52dSVZdiIpCCiExflmWtvR9dGRLF1JBAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwKMwfP5g0ustfct6d9SAvQD27sD/sPeAwN59gL37YLEXTWbjMmByLn6bnetf6rPzgUPNCOZapdhkdlKGLlFZau99zhh7LW6zi2CXsU1eS8pYS6mNeHHSq2rIxGIJjr6TlhKLDvnt5aARO1T87DVY8mYvZVuixo3j5M071gkyurwSUTUW9/fypNpyZc8MHjL2a2c7l8Kn7PJgL2+FsUPFz95uPOYt3RVvZhs5/226WaeiqYxMef8E9fyPxZ4RnL0cU3Dapv/ZlXjxZexQ8bLXkCajvEPyLRIOk2NxYQPRbKoxOI5Ne2bwB1UHBQ/lG7zbHCpe9obqOaPmZfIbs7Sny7O2KI6k/pw9M1hRJ6/dC/mQxg4Vuz1WQoqFSshIJRH3XXlJqfq0I8u7W4Y9M1g3/Ez+E2GxQ8XLns4uJCe6kA9HdbmotDfbMOwZwRo+KEUPEs3YoWK3N/9xtYpoYCa3ItNn8nJBQ1MVr1V2KcqeEazh37uRFBXGDhUfe2GhIpqfqao29KVWDOb1tmHPCLbaIwWrio+9dT2XqDbV91lZow6vbRr2jGAimvR3dvjH1R1GClYVH3tN/Uh1mjU2C71a+d4ye2IJolEdFaNmbdOh4mVPN8dhL7mW5F72Riw++HrygXJNribp9LlD5Zb22lZ7eppXMqrTXpT/Fn1G9qZyFoh8b1W5pb3lH7eK014zll+U7NVk4jul1dKq4mXPd+x52dMrRY9SVrI/+PiN0TJ7Z3vl4w0z18tesVLQXqHZSulr/l6o9Hb2FvKeUWNZ7mGvXiQTUt7din5s7+yLZ6uKj73G/Kph1KjTl4+9IhWLXVB6Vr5gVfGxt7DmGjVG7CwwuMne+qI9q4qXvfkdi3u0e9lTnZSJj3tVecOm4mVvqCaVuFrsZZZJ57KnOilKaOY22eXs64kaOjYVL3vzu2XTXmg5bjpnbio266ftLr1zKPKw2N5bVbzsBV1xyCrOGkaNQTcWFXwee9jLSGXEesJeMNnbfpEzmaktKn72Qn5S66fypGWzR+UzfpTzsReymB/a/gbSnuCTyMs2FT97wahyznVuQpK3FQ33luCUEvGgai+Yxg4VX+qzg/Gy8mjW9zngC476Pxd+CVVmvY3KY+Kd81YDLcBPmPxJ997NR++VEnpvB1aC93r2qDRkJuWd11mpDwcNdskzYPQlv/GPUqvhG18lrvlC8VTn7dF2zhK5YQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAj8Q/yA/zA3+AImAAAAAASUVORK5CYII='
}

export default class Factory extends FixtureFactory {
  name = 'home'
  model = {
    title: (fixtures) => `${_.upperFirst(fixtures.type.replace('-', ' '))} in ${fixtures.city.name}`,
    user: 'ref.user',
    city: 'ref.city',
    slug: fixtures => faker.helpers.slugify(`${fixtures.id}-Home in ${fixtures.city.name}`),
    type: {
      method: 'random.arrayElement',
      args: [['home', 'room', 'shared-room']]
    },
    pricePerNight: {
      method: 'random.number',
      args: [{min: 20, max: 80}]
    },
    bedCount: {
      method: 'random.number',
      args: [{min: 1, max: 4}]
    },
    rating: {
      method: 'random.number',
      args: [{min: 2, max: 7}]
    }
  }
  instances = 5
}
