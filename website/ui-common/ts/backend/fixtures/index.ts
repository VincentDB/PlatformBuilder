const _ = require('lodash')
import * as moment from 'moment'
import * as faker from 'faker'
import * as fixtureFactory from 'fixture-factory'
import * as objectPath from 'object-path'

let FACTORY_INSTANCES = null

export class FixtureFactory {
  public name : string
  public model
  protected instances : number | Array<object> = 0
  private generated = []
  private idsGenerated = 0

  register() {
    fixtureFactory.register(this.name, this.model)
  }

  createDefaultInstances() {
    // console.log('ceewefe', this.name)

    if (_.isNumber(this.instances)) {
      _.times(this.instances, () => this.create())
    } else if (this.instances instanceof Array) {
      // console.log('!', this.instances)
      for (const overrides of this.instances) {
        this.create(overrides)
      }
    }
  }

  create(overrides?) {
    const instance = fixtureFactory.generateOne(this.model, {
      id: this.generateId(),
      ...overrides
    })
    this.generated.push(instance)
    // console.log(instance)
    return instance
  }

  get(query = null) {
    let result
    if (_.isNumber(query)) {
      result = this.generated[query]
    } else if (query === null) {
      result = this.get(0)
    } else {
      result = _.find(this.generated, query)
    }
    return result
  }

  getAll() {
    return this.generated
  }

  findAll(query) {
    return _.filter(this.generated, query)
  }

  generateId() {
    return `id-${this.name}-${this.idsGenerated++}`
  }
}

export function createFixtureFactories({factories = null} = {}) {
  faker.seed(3) // Predictable randomness FTW!
  fixtureFactory.removeAllListeners()
  fixtureFactory.reset()
  
  factories = factories || _.map([
    require('./users'),
    require('./cities'),
    require('./homes'),
    require('./projects'),
    require('./home-page'),
  ], 'default')

  FACTORY_INSTANCES = _.fromPairs(factories.map(factoryClass => {
    const factory = new factoryClass()
    // factory.register()
    return [factory.name, factory]
  }))

  installFixtureFactoryListener(FACTORY_INSTANCES)

  _.forEach(FACTORY_INSTANCES, factory => {
    factory.createDefaultInstances()
  })
}

export function getFixtureFactories({reset = false, factories = null} = {}) {
  if (FACTORY_INSTANCES && !reset) {
    return FACTORY_INSTANCES
  }

  createFixtureFactories({factories})

  return FACTORY_INSTANCES
}

export function getFixtureFactory(name) {
  return getFixtureFactories()[name]
}

export function installFixtureFactoryListener(factoryInstances) {
  fixtureFactory.on('field:pre', ({name : fieldName, model}) => {
    if (!_.isString(model.method)) {
      return
    }
    
    const parts = model.method.split('.')
    if (parts[0] === 'ref') {
      model.method = (fixture, options, dataModel) => {
        const factory = factoryInstances[parts[1]]
        const instances = factory.getAll()
        const instance = faker.random.arrayElement(instances)
        return instance
      }
    } else if (parts[0] === 'moment') {
      model.method = () => {
        let result
        if (parts[1] === 'soon') {
          result = moment().add(
              faker.random.number({min: 1, max: model.args[0].days}), 'days'
          ).toDate()
        } else {
          result = faker.date[parts[1]](...model.args)
        }
        // model.method = result
        return moment(result)
        // console.log(model.method.format())
      }
    }
  })

  fixtureFactory.installedListeners = true
}
