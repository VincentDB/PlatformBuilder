// https://github.com/peterKaleta/fixture-factory
const _ = require('lodash')
import * as faker from 'faker'
import * as fixtureFactory from 'fixture-factory'
import { FixtureFactory } from './'

const DUMMY_IMAGE_URLS = {
  'list': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATsAAADNBAMAAAA1TspEAAAAG1BMVEUAAAD///+/v7/f399/f38/Pz+fn59fX18fHx+TUjWGAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAEOElEQVR4nO2YS1fbOBiGRYhjLUc2GJYxpaddYnLasgynmTDLZEoOWSYtDSwTeglLe04h/OzRzTeIkwgbVu+zMLb06fMTWZJlCAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAN9bebRNkbRT0LnzHWOuwV1DZ2N8lR36tQKI8/mUy+Bu50eW2hnv1P5uIl9UJ+oKc7y2sL9Rr7mYuX1iM0KHi8YUGznF5hVHmUHuk3zZrl9V4OrbdteLtX1ttsiqa8sl59l1j8jA7k1Xn3TNVSNSStbkePTV7Te6JHhyImJD9Fqz+XahVYdCZnKveiy0+G+TyGerz3oia5D5i4+MFcthPqYuEeuIHbUzWcHql3PnudTnIrOXOj0Q+XfSINpkK/iUi5HtyzVuD0jnN5TPVq+1yvzg7nwsm9Jdb4JNXzv3OxXV1Df/fIFpPEi6XSu/JCOwj9KZnxnqXO3yFZBG2xKFwT8u/ecTaPsR7vuqjpX8vz0xE/2G6i1xCdQAMeOB7FzZ6uexEXI/1L3qAu4mVaMeFqclEcO9k8xnrjKYkO1epKXSXZjvX6UqrfJpabNFui5/FDTfaom61QrWtOJo+xXoO3jgLVsKEst45iPRWyfZSd3kv0REldmvlxhcX7aywHm+1k8pjqnQfveX5Hlej26rmKg+qM+r4yLtRrCgs5GcahrqBe7EqdTB4TvVarFfAZx/PrN6e8j76T0BP3ENd7cU2B3kgFkbjDHuuleUz0Dg4OJr+k1l+qRA8Oy9N6ltouWHvZYbNej3ZbjHlxV/JsaR4TvTA+i/SjmykJ8Tiknu0eSHbimo30LN85m//04nR8QKd5nqfXXK33zkhvJsaLeJ7bcj7NjjJ5qtVLfq6BHnVDrUcZ37ne80vbqNsK9dTy9GTspTWb6Kk1SOSoO+z4DbvO5imlp//KO8mZ6zyO2EBPrU62J8oX3ctbks1TSm9bzeB03QviiMymcJ3elkxW43r95DMmIOY81dNfDlHy1jhNXv3p48ntD5fpyd7re7pckuQppUdbsmg8jS2iEx1BWRKc+/pZolcTvUuPPbHLufo1kO2SPKX0yEykacg1XurZchaKPcjsQxxseav1xDuWfDkRUTdi7/Uul6eUns2+P5wHo0SPzLzbh4tvU1FzNbz43BZlAY9ZoUf8T4Mb50H+CHo3+M3eZ/OU0uP7W/VzYz065gUfQ13jyPz/8bNVeg3G3DZN+lg+jDRPOaz5kGT0CBnEBdbFQMfczVdvy+/mOQ+1C03yVIPpB1wxp4bfQBtRq+x78Tmrylr0El0ByRa1Skz/t1EIddfHmCcNyj+SW3n8UtkgTlh0fW991Dr8wyGhN6z6oddnHyuYbn/E/1/d6/KJHkPDatJcdM5eYlUBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8Jr8D0PFzBIcxx2BAAAAAElFTkSuQmCC'
}

export default class Factory extends FixtureFactory {
  name = 'project'
  model = {
    city: 'ref.city',
    slug: (fixtures) => faker.helpers.slugify(fixtures.name),
    user: 'ref.user',
    name: {
      method: 'fake',
      args: ['Project #{{random.number}}']
    },
    short_description: (fixtures) => `A very cool short description about ${fixtures.name}`,
    deadline: {
      method: 'moment.soon',
      args: [{days: 14}]
    },
    goal: {
      method: 'random.number',
      args: [{min: 10000, max: 40000}]
    },
    raised: fixtures => faker.random.number({
      min: fixtures.goal / 100 * 40,
      max: fixtures.goal / 100 * 100
    })
  }
  instances = 50
}
