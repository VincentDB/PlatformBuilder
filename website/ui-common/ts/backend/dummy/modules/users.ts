const _ = require('lodash')
import Backend from '../'
import UsersBackend from '../../common/modules/users'

export default class DummyUsersBackend extends UsersBackend {
  constructor(private backend : Backend) {
    super()
  }

  getUserByID(id) {
    return this.backend._delayedReturn(this.backend._tables.user.get({id: id}))
  }

  getSemiRandomUser() {
    return this.backend._delayedReturn(this.backend._tables.user.get())
  }
}
