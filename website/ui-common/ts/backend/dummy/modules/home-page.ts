const _ = require('lodash')
import HomePageBackend from '../../common/modules/home-page'

export default class DummyHomePageBackend extends HomePageBackend {
  private featured
  private stats

  constructor(private backend) {
    super()

    this.featured = _.mapValues(
      this.backend._tables['home-page'].get().featured,
      (collection) => _.map(collection, 'object')
    )
    this.stats = this.backend._tables['home-page'].get().stats
  }

  fetchFeatured() {
    return this.backend._delayedReturn(this.featured)
  }

  retrieveStats() {
    return this.backend._delayedReturn(this.stats)
  }
}
