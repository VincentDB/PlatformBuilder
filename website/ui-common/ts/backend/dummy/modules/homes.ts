const _ = require('lodash')
import Backend from '../'
import HomesBackend from '../../common/modules/homes'

export default class DummyHomesBackend extends HomesBackend {
  constructor(private backend : Backend) {
    super()
  }

  createHome(home) {
    this.backend._tables.home.create({
      ...home,
      user: home.user.id,
      rating: 5,
    })
    return this.backend._delayedReturn(null)
  }

  updateHome(home) {
    this.backend._tables.home.update(home)
    return this.backend._delayedReturn(null)
  }

  saveHome(home) {
    if (home.id) {
      return this.updateHome(home)
    } else {
      return this.createHome(home)
    }
  }

  findHomesByUser({user}) {
    if (!user) {
      return Promise.resolve(null)
    }

    const result = this.backend._tables.home.findAll({user: user.id})
    return this.backend._delayedReturn(result)
  }

  findHomesToStay(
    {city, fromDate, toDate} :
    {city : object, fromDate? : object, toDate? : object}
  ) {
    return this.backend._delayedReturn(
      this.backend._tables.home.findAll({city: city['id']})
    )
  }
}
