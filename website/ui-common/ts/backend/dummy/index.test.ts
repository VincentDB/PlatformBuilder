import Backend from './'
import testBackend from '../test'

describe('Dummy backend', () => {
  testBackend(() => {
    return new Backend()
  })
})
