const _ = require('lodash')
import * as moment from 'moment'
import DatabaseTable from './database'
import { getFixtureFactories } from '../fixtures'

import HomePageBackend from '../common/modules/home-page'
import UsersBackend from '../common/modules/users'
import HomesBackend from '../common/modules/homes'

export default class DummyBackend {
  public homePage : HomePageBackend
  public users : UsersBackend
  public homes : HomesBackend

  public _tables : {[name : string] : DatabaseTable} = {}

  constructor() {
    Object.assign(this._tables, _.mapValues(getFixtureFactories(), (factory, name) => {
      return new DatabaseTable(this._tables, name, factory)
    }))

    this.homePage = new (require('./modules/home-page').default)(this)
    this.users = new (require('./modules/users').default)(this)
    this.homes = new (require('./modules/homes').default)(this)
  }

  async reset() {

  }

  _delayedReturn(value) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(value)
      }, 1000)
    })
  }

  _createMoment() {
    return moment.apply(moment, arguments)
  }
}
