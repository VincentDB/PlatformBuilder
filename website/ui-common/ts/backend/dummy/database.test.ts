const _ = require('lodash')
import { expect } from 'chai'
import { getFixtureFactories, FixtureFactory } from '../fixtures'
import DatabaseTable from './database'

export class FooFactory extends FixtureFactory {
  name = 'foo'
  model = {
    displayName: {
      method: 'fake',
      args: ['{{name.firstName}} {{name.lastName}}']
    }
  }
  instances = 1
}

export class BarFactory extends FixtureFactory {
  name = 'bar'
  model = {
    foo: 'ref.foo',
    displayName: {
      method: 'fake',
      args: ['{{name.lastName}} {{name.firstName}}']
    }
  }
  instances = 1
}

describe('Database class', () => {
  let tables : {[name : string] : DatabaseTable}
  let fixtureFactories

  beforeEach(() => {
    tables = {}
    fixtureFactories = getFixtureFactories({reset: true, factories: [FooFactory, BarFactory]})
    Object.assign(tables, _.mapValues(fixtureFactories, (factory, name) => {
      return new DatabaseTable(tables, name, factory)
    }))
  })

  it('should be able to retrieve, modify and delete objects', () => {
    const foo = tables['foo'].get()
    expect(foo).to.deep.equal({
      id: 'id-foo-0',
      displayName: 'Kathryn Bernier'
    })

    const bar = tables['bar'].get()
    expect(bar).to.deep.equal({
      id: 'id-bar-0',
      displayName: 'Spencer Eileen',
      foo: {
        id: 'id-foo-0',
        displayName: 'Kathryn Bernier'
      }
    })

    foo.displayName = 'Jane Doe'
    expect(bar).to.deep.equal({
      id: 'id-bar-0',
      displayName: 'Spencer Eileen',
      foo: {
        id: 'id-foo-0',
        displayName: 'Kathryn Bernier'
      }
    })

    tables['foo'].update({...foo, displayName: 'Janis Doe'})
    expect(tables['foo'].get()).to.deep.equal({
      id: 'id-foo-0',
      displayName: 'Janis Doe'
    })
    expect(tables['bar'].get()).to.deep.equal({
      id: 'id-bar-0',
      displayName: 'Spencer Eileen',
      foo: {
        id: 'id-foo-0',
        displayName: 'Janis Doe'
      }
    })
    expect(foo).to.deep.equal({
      id: 'id-foo-0',
      displayName: 'Jane Doe'
    })
    expect(bar).to.deep.equal({
      id: 'id-bar-0',
      displayName: 'Spencer Eileen',
      foo: {
        id: 'id-foo-0',
        displayName: 'Kathryn Bernier'
      }
    })

    tables['foo'].remove(foo)
    expect(tables['foo'].get()).to.equal(null)
    expect(tables['bar'].get()).to.equal(null)
    expect(foo).to.deep.equal({
      id: 'id-foo-0',
      displayName: 'Jane Doe'
    })
    expect(bar).to.deep.equal({
      id: 'id-bar-0',
      displayName: 'Spencer Eileen',
      foo: {
        id: 'id-foo-0',
        displayName: 'Kathryn Bernier'
      }
    })
  })
})
