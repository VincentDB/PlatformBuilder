const _ = require('lodash')
import visit from 'rapid-ui/ts/utils/visit-values'
import * as deepcopy from 'deepcopy'
import { FixtureFactory } from '../fixtures'

export default class DatabaseTable {
  // Small class that does copy on write with fixture backend

  private modified : {[id : string] : object} = {}
  private created : Array<object> = []
  private removed : {[id : string] : boolean} = {}

  constructor(
    private tables : {[name : string] : DatabaseTable},
    private tableName : string,
    private factory : FixtureFactory
  ) {
  }

  create(obj) {
    const id = this.factory.generateId()
    obj = {...obj, id}
    this.created.push(obj)
    this.modified[id] = obj
  }

  get(query = null) {
    if (query === null) {
      return this.get(0)
    }

    let result
    if (_.isNumber(query)) {
      result = this.factory.get(query)
      if (result && this.modified[result.id]) {
        result = this.modified[result.id]
      }
    } else {
      // this.tableName === 'home' && console.log(this.modified, query)
      result = result || _.find(this.modified, query)
      result = result || this.factory.get(query)
    }
    result = result && !this.removed[result.id] ? result : null

    if (result) {
      result = deepcopy(result)

      let deleted = false
      visit(result, (value, key, parent) => {
        // if (key === 'id' || !_.isString(value) || value.indexOf('id-') === -1) {
        //   return
        // }

        // const [prefix, type, ...rest] = value.split('-')
        // parent[key] = this.tables[type].get({id: value})

        if (key === 'id' && parent !== result && !deleted) {
          const [prefix, type, ...rest] = value.split('-')
          const ref = this.tables[type].get({id: value})
          if (!ref) {
            deleted = true
            return
          }
          Object.assign(parent, ref)
        }
      })
      if (deleted) {
        result = null
      }

      // for (const key in result) {
      //   const value = result[key]
      //   console.log(key, value)
      //   if (!_.isString(value) || value.indexOf('id-') === -1) {
      //     continue
      //   }

      //   const [prefix, type, ...rest] = value.split('-')
      //   result[key] = this.tables[type].get([type, ...rest].join('-'))
      // }
    }
    return result
  }

  findAll(query) {
    const fromFixtures = _.mapKeys(this.factory.findAll(query), 'id')
    const fromModified = _.mapKeys(_.filter(this.modified, query), 'id')
    Object.assign(fromFixtures, fromModified)
    const result = _.filter(fromFixtures, query)
    return result
  }

  // getAll() {
  //   const ids = _.map(this.factory.getAll(), 'id')
  //   ids.push(..._.map(this.created, 'id'))
  //   return ids
  // }

  update(obj) {
    this.modified[obj.id] = obj
  }

  remove(obj) {
    this.removed[obj.id] = true
  }
}