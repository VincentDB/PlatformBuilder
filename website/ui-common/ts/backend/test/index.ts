import {expect} from 'chai'
// import moment from 'moment'
import testHomes from './homes'

export default function testBackend(createBackend) {
  let backend = createBackend()

  beforeEach(async () => {
    await backend.reset()
  })

  testHomes({backend})

  // it('should be able to add and search', async () => {
  // })
}
