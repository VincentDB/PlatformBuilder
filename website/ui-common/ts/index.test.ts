import * as path from 'path'
import { testApp, testTools } from 'rapid-ui/ts/test'

testApp(path.resolve(__dirname, '../../ui-data'))
if (process.env.TEST_TOOLS === 'true') {
  testTools()
}
