export default class LocalStorage {
  constructor(private storage) {
  }

  async getItem(key) {
    this.storage.setItem(key)
  }
  async setItem(key, val) {
    this.storage.setItem(key, val)
  }
  async removeItem(key) {
    this.storage.removeItem(key)
  }
  async popItem(key) {
    const val = await this.getItem(key)
    await this.removeItem(key)
    return val
  }
}
