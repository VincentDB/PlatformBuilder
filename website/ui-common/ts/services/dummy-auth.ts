import { EventEmitter } from 'events'


export default class DummyAuthService {
  private backend
  public user
  public events
  
  constructor(backend) {
    this.backend = backend
    this.user = null
    this.events = new EventEmitter
  }

  async init() {
    const userID = window.localStorage.getItem('user-id')
    if (userID) {
      let user = await this.backend.users.getUserByID(userID)
      if (!user) {
        console.warn('User %s could not be found', userID)
        user = null
      }
      this.setUser(user)
    } else {
      this.setUser(null)
    }
  }

  setUser(user) {
    this.user = user
    if (user) {
      window.localStorage.setItem('user-id', user.id)
    } else {
      window.localStorage.removeItem('user-id')
    }
    this.events.emit('changed')
  }

  // async loginAnonymousIfNecessary() {
  //   if (this._user && !this._user.anonymous) {
  //     return
  //   }

  //   this._user = {
  //     id: '__ANON__',
  //     anonymous: true,
  //   }
  //   return this._user
  // }

  async loginWithProvider(provider) {
    this.setUser(await this.backend.users.getSemiRandomUser())
    return this.user
  }

  async logout() {
    this.setUser(null)
  }
}
