import LocalStorage from './local-storage'

export default function create(backend, backendType) {
  const services = {
    auth: null,
    localStorage: new LocalStorage(window.localStorage),
    sessionStorage: new LocalStorage(window.sessionStorage),
  }
  if (backendType === 'firebase') {
    const firebase = require('firebase')
    services.auth = new (require('./firebase-auth').default)(firebase)
  } else {
    services.auth = new (require('./dummy-auth').default)(backend)
  }
  return services
}
