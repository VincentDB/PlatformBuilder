const _ = require('lodash')
import * as fs from 'fs'
import * as path from 'path'
import * as yaml from 'js-yaml'
import {testAppState} from './redux/test'
import {testAppStructure, testAppStructureDir} from './structure/test'
// import * as yaml from 'js-yaml'
// import * as AJV from 'ajv'

// export function loadScheme(path) {
//   const scheme = yaml.safeLoad(fs.readFileSync(path), {
//     filename: path
//   })
//   const validator = new AJV()
//   _.each(scheme.types, (type, key) => {
//     const id = `/${key}`
//     validator.addSchema({id, ...type}, id)
//   })
//   const validate = validator.compile(scheme.scheme)
//   return {validate: inst => {
//     const valid = validate(scheme.scheme)
//     if (!valid) {
//       return {errors: validate.errors, errorsText: validator.errorsText(validate.errors)}
//     }
//     return {errors: null}
//   }}
// }

export function collectSuites(suiteDataDir, {prefix = null} = {}) {
  // const structureScheme = loadScheme(path.resolve(__dirname, '../../schemes/structure.yaml'))
  const suitePaths = fs.readdirSync(suiteDataDir)
  const suites = []
  suitePaths
    // .slice(0, 1)
    .forEach(suiteFileName => {
      if (path.extname(suiteFileName) !== '.yaml') {
        return
      }

      const suitePath = path.resolve(__dirname, suiteDataDir, suiteFileName)
      const suite = yaml.safeLoad(fs.readFileSync(suitePath), {
        filename: suitePath
      })
      if (!suite || !suite.tests) {
        console.log(`Could not load suite ${suiteFileName}`)
        return
      }
      // if (process.env.TEST_ONLY && process.env.TEST_ONLY !== `structure:${component}`) {
      //   return
      // }

      const moduleName = suiteFileName.split('.').slice(0, -1).join('.')
      const info = {
        moduleName: prefix ? `${prefix}:${moduleName}` : moduleName
      }
      suites.push({suite, info})
    })
  return suites
}

export function testApp(dataDir, {createBackend, createServices}) {
  testAppStructure(path.resolve(dataDir, 'structure'))
  testAppState(path.resolve(dataDir, 'state'), {createBackend, createServices})
}

export function testTools() {
  testAppStructureDir(path.resolve(__dirname, '../structure-tests'), 'tools')
  testAppState(path.resolve(__dirname, '../state-tests'), {
    prefix: 'tools',
    createBackend: () => ({}),
    createServices: () => ({})
  })
}
