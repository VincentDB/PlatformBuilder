const _ = require('lodash')
import * as fs from 'fs'
import * as path from 'path'
import { stub } from 'sinon'
import { expect } from 'chai'
import { renderStructure } from './index'
import * as yaml from 'js-yaml'
import * as objectPath from 'object-path'
import { collectSuites } from '../test';

export function testSuite(suite) {
  suite.tests.forEach((test, testIdx) => {
    it(test.should || `test ${testIdx + 1}`, () => {
      const actionStubs = {}
      const eventHandlers = {}
      const structure = _.omit(suite, 'tests')
      const mergedStructure = {
        ...structure,
        ...(test.content ? {content: test.content} : {})
      }

      const rendered = renderStructure({
        structure: mergedStructure,
        path: test.path || '/',
        withKeys: !!test.keys,
        input: test.attrs || {},
        inherit: test.context || {},
        renderStateNode: ({definition, render}) => {
          const state = _.fromPairs((definition.state || []).map(path => {
            path = path.split(/\.|:/)
            return [_.last(path), objectPath.get(test.state, path)]
          }))

          const actions = _.fromPairs((definition.actions || []).map(action => {
            const name = action.split(':')[1]
            const actionStub = stub()
            actionStubs[action] = actionStub
            return [name, actionStub]
          }))

          return render({state, actions})
        },
        renderNode: ({definition, generated, handlers, context}) => {
          eventHandlers[definition.type] = handlers

          const rendered : any = {
            type: definition.type,
          }
          if (definition.name) {
            rendered.name = definition.name
          }
          if (generated.children) {
            rendered.children = generated.children
          }
          if (generated.content) {
            rendered.content = generated.content
          }
          if (generated.attrs && !_.isEmpty(generated.attrs)) {
            rendered.attrs = generated.attrs
          }
          if (generated.styles) {
            rendered.styles = generated.styles
          }
          if (generated.key) {
            rendered.key = generated.key
          }
          
          return rendered
        }
      })

      if (test.event) {
        eventHandlers[test.target][test.event](test.info)

        const actionStub = actionStubs[test.triggered.action]
        expect(actionStub.calledOnce).to.be.true

        const actionArgs = actionStub.getCall(0).args
        expect(actionArgs).to.deep.equal([_.omit(test.triggered, 'action')])

        return
      }

      if (test.result) {
        expect(_.omit(rendered, 'type')).to.deep.equal(test.result)
      } else {
        console.log(`Found test without expected result: test ${testIdx + 1}`)
        console.log(`Current output of test is:`)
        console.log(yaml.safeDump({result: _.omit(rendered, 'type')}))
      }
    })
  })
}

export function testAppStructure(structureDataDir) {
  // const structureScheme = loadScheme(path.resolve(__dirname, '../../schemes/structure.yaml'))
  testAppStructureDir(structureDataDir)
  
  fs.readdirSync(structureDataDir).forEach(moduleDirName => {
    const moduleDir = path.join(structureDataDir, moduleDirName)
    if (!fs.statSync(moduleDir).isDirectory()) {
      return
    }
  
    testAppStructureDir(moduleDir, moduleDirName)
  }) 
}

export function testAppStructureDir(dirPath, module = null) {
  const suites = collectSuites(dirPath, {prefix: module})
  suites.forEach(({suite, info}) => {
    suite.type = info.moduleName

    const description = `structure suite: ${info.moduleName}`

    describe(description, () => {
      testSuite(suite)
    })
  })
}