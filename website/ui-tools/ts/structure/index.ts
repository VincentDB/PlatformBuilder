const _ = require('lodash')
import * as Route from 'route-parser'
import * as jsonLogic from '../utils/logic'
import { interpolate } from '../utils/string'

export function preprocessStructure(structure) {
  const actions = _.fromPairs((structure.actions || []).map(action => [action, true]))
  _.each(structure.handlers || {}, (handler, eventName) => {
    const [module, actionName] = handler.action.split(':')
    if (actionName) {
      actions[handler.action] = true
      handler.action = actionName
    }
  })
  if (!_.isEmpty(actions)) {
    structure.actions = Object.keys(actions)
  }

  _.each(_.isArray(structure.content) ? structure.content : [], preprocessStructure)
  _.each(_.isArray(structure.children) ? structure.children : [], preprocessStructure)

  return structure
}

export function renderStructure({
  structure,
  renderNode,
  renderStateNode,
  path,
  withKeys = false,
  // key = 'root',
  index = 0,
  state = {},
  input = {},
  parent = {},
  context = {},
  inherit = {},
} : {
  structure,
  renderStateNode,
  renderNode,
  path : string,
  withKeys? : boolean,
  // key? : string,
  index? : number,
  state? : any,
  input? : any,
  parent? : any,
  context? : any,
  inherit? : any
}) {
  context = {...context, ...inherit, content: null, input, parent, state}

  context.path = path
  if (structure.route) {
    const route = new Route(structure.route.path || structure.route)
    context.route = route.match(path)
  }
  
  let nodeActions = null
  const render = () => {
    context.content = renderContent({
      structure, contentDefinition: structure.content,
      renderStateNode, renderNode, context, inherit,
      withKeys
    })

    const children = renderContent({
      structure, contentDefinition: structure.children,
      renderStateNode, renderNode, context, inherit,
      withKeys
    })
    const attrs = _.omit(input, Object.keys(structure.input || {}))
    
    let key = null
    if (withKeys) {
      if (structure.id) {
        key = interpolate(structure.id, context)
      } else if (structure.repeat && inherit[structure.repeat.target].id) {
        key = inherit[structure.repeat.target].id
      } else if (!structure.repeat) {
        key = `${structure.type}-${index}`
      } else {
        console.error(`Could not determine key for ${structure.type} with context`, context)
      }
    }

    const styles = getStyles({structure, context})
    
    return renderNode({
      definition: structure,
      generated: {
        content: context.content,
        children,
        styles,
        attrs,
        key
      },
      handlers: _.mapValues(structure.handlers || {}, (handlerDefinition, eventName) => {
        return buildEventHandler({
          handlerDefinition,
          eventName,
          context,
          action: nodeActions[handlerDefinition.action]
        })
      }),
      context
    })
  }

  if (structure.state || structure.actions) {
    const isStaticState = !_.isArray(structure.state)
    const requestedState = !isStaticState ? structure.state : []

    return renderStateNode({
      definition: {...structure, state: requestedState},
      key: `state-${index}`,
      render: ({state, actions}) => {
        if (!isStaticState) {
          context.state = {...context.state, ...state}
        } else {
          context.state = {...context.state, ...structure.state}
        }
        nodeActions = actions
        return render()
      }
    })
  } else {
    return render()
  }
}

export function shouldRender({definition, context}) {
  if (!definition.conditional) {
    return true
  }

  let shouldShow
  if (typeof definition.conditional === 'string') {
    if (definition.conditional.indexOf('$') === 0) {
      shouldShow = interpolate(definition.conditional, context)
    } else {
      shouldShow = this.props.logic.execute(
        definition.conditional, 'conditional', context
      )
    }
  } else {
    shouldShow = jsonLogic.apply(
      definition.conditional,
      context
    )
  }

  return shouldShow
}

export function renderContent({
  structure, contentDefinition, renderStateNode, renderNode, context, inherit, withKeys
}) {
  if (!contentDefinition) {
    return null
  }

  if (typeof contentDefinition === 'string') {
    const content = interpolate(contentDefinition, context)
    if (!content) {
      console.error(`Could not resolve ${contentDefinition} for ${structure.type} in context`, context)
    }
    return content
  }

  const content = []
  filterChildrenForRoutes(contentDefinition, context.path).forEach((child, idx) => {
    const shouldRenderChild = (extraContext = {}) => {
      return shouldRender({
        definition: child,
        context: {
          ...context,
          ...extraContext
        }
      })
    }
    
    const maybeRenderChild = (childInherit = {}) => {
      if (!shouldRenderChild(childInherit)) {
        return
      }

      const childInput = _.mapValues(child.attrs || {}, (val) => {
        const childContext = {...context, ...childInherit}
        if (typeof val === 'string') {
          return interpolate(val, childContext)
        } else {
          return jsonLogic.apply(val, childContext)
        }
      })

      content.push(renderStructure({
        structure: child,
        renderStateNode,
        renderNode,
        path: context.path,
        input: childInput,
        parent: context,
        index: idx,
        inherit: {...inherit, ...childInherit},
        state: context.state,
        withKeys
      }))
    }

    if (child.repeat) {
      const collection = interpolate(child.repeat.source, context)
      if (!collection) {
        console.error('Could not resolve repeat source:', child.repeat.source)
      }

      // console.log('repeat', child.repeat.source, collection)
      _.each(collection || [], (dataElement, key) => {
        if (_.isPlainObject(dataElement) && _.isString(key)) {
          dataElement = {
            ...dataElement,
            name: key
          }
        }
        maybeRenderChild({[child.repeat.target]: dataElement})
      })
    } else {
      maybeRenderChild({})
    }
  })
  return content
}

export function filterChildrenForRoutes(children, path) {
  let routeMatched = false
  return (children || []).map(child => {
    if (routeMatched && child.route) {
      return
    }
    
    if (child.route) {
      const route = new Route(child.route.path || child.route)
      const params = route.match(path)
      if (params) {
        routeMatched = true
      } else {
        return
      }
    }

    return child
  }).filter(child => !!child)
}

export function buildEventHandler({handlerDefinition, eventName, context, action}) {
  return (event) => {
    const eventContext = {
      ...context,
      event: {type: eventName, ...event}
    }
    const interpolateParams = (params) => {
      return _.mapValues(params, (value, key) => {
        if (_.isPlainObject(value)) {
          return interpolateParams(value)
        }
        return interpolate(value, eventContext)
      })
    }
    const params = interpolateParams(_.omit(handlerDefinition, 'action'))
    action(params)
  }
}

export function getStyles({structure, context}) {
  if (!structure.style) {
    return null
  }
  
  const styles = []
  const pushStyle = style => {
    styles.push(_.mapValues(style, val => {
      return interpolate(val, context)
    }))
  }
  pushStyle(_.omit(structure.style, 'conditionals'))

  const conditionals = structure.style.conditionals || []
  for (const conditional of conditionals) {
    if (typeof conditional['if'] === 'string' &&
        !interpolate(conditional['if'], context)) {
      continue
    }
    if (!jsonLogic.apply(conditional['if'], context)) {
      continue
    }
    pushStyle(_.omit(conditional, 'if'))
  }

  return styles
}
