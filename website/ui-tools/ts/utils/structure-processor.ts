export default function processStructure(
  structure, {components, parents = null}
) {
  if (!parents) {
    parents = new WeakMap()
  }

  if (structure.type === 'html:text') {
    structure.type = 'html:span'
  }

  // structure.getChildren = ({andSelf, onlyDirect}) => {
  //   return getChildren({component: structure, components, andSelf, onlyDirect})
  // }

  for (const child of (structure.children || [])) {
    if (typeof child === 'string') {
      continue
    }

    if (!child.parent) {
      parents.set(child, structure)
      Object.defineProperties(child, {
        parent: {
          get: () => {
            return parents.get(child)
          }
        }
      })
      child.getParents = ({start, andSelf}) => {
        return getParents({component: child, start, andSelf})
      }
    }
    processStructure(child, {components, parents})
  }

  return structure
}

export function getParents({component, start, andSelf}) {
  const parents = []

  let structure = andSelf ? component : component.parent
  while (structure) {
    if (start === 'top') {
      parents.unshift(structure)
    } else if (start === 'bottom') {
      parents.push(structure)
    } else {
      throw new Error(
        'child.parents() must receive "top" or "bottom" for start'
      )
    }
    structure = structure.parent
  }
  return parents
}

// export function getChildren({component, components, andSelf, onlyDirect}) {
//   const children = []
//   if (!(component instanceof Array)) {
//     return children
//   }

//   if (andSelf) {
//     children.push(component)
//   }

//   for (let child of (component.children || [])) {
//     children.push(child)
//     if (!onlyDirect || isDirectComponent(child, components)) {
//       children.push(...getChildren({
//         component: child, components, andSelf: false, onlyDirect
//       }))
//     }
//   }
//   return children
// }

export function isDirectComponent(definition, components) {
  const component = components.find(definition.type)
  const direct = !!component || !!definition.direct
  return direct
}
