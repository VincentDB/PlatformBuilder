export default function visit(current, fn) {
	for (let i = 0, keys = Object.keys(current); i < keys.length; i++) {
		let key = keys[i]
		let value = current[key]

		if (value === undefined || value === null) {
			continue
		}

		if (typeof value === 'object' || typeof value === 'function') {
			visit(current[key], fn)
			continue
		}

		let proceed = fn(current[key], key, current)

		// returning false (and only false)
		// from the visitor function will stop the 
		// visitations
		if (proceed === false) {
			break
		}
	}
}
