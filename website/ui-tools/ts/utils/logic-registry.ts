import * as _ from 'lodash'
import * as objectPath from 'object-path'
import * as jsonLogic from 'json-logic-js'
import visit from './visit-values'
import * as deepcopy from 'deepcopy'

export default class LogicRegistry {
  private byName

  constructor(definition) {
    this.byName = {}
    definition.forEach(func => {
      this.byName[func.name] = func
    })
  }

  find(name, type) {
    const func = this.byName[name]
    if (!func) {
      throw new Error(`No such ${type}:${name}`)
    }
    if (func.type !== type) {
      throw new Error(`Function ${name} is not a(n) ${type}, but an ${func.type}`)
    }
    return func
  }

  findAndTransform(name, type) {
    const func = this.find(name, type)
    return {...func, rules: transformRule(func.rules)}
  }

  execute(name, type, context) {
    const func = this.find(name, type)
    const inputPairs = _.map(func.input, elem => {
      const path = elem.state.substr(1).split('.')
      return [
        _.last(path),
        objectPath.get(context.state, path)
      ]
    })
    const input = _.fromPairs(inputPairs)
    const output = jsonLogic.apply(
      func.rules,
      input
    )

    return output
  }
}

export function transformRule(rule) {
  const copy = deepcopy(rule)
  visit(copy, (value, key, parent) => {
    if (key === 'var') {
      parent.var = _.camelCase(value)
    }
  })
  return copy
}
