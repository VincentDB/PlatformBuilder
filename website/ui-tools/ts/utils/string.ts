import * as _ from 'lodash'
import * as objectPath from 'object-path'
import * as Immutable from 'immutable'

export function interpolate(s, context) {
  if (typeof s !== 'string') {
    return s
  }

  if (s && s.substr(0, 1) === '$' && s.substr(0, 2) !== '${') {
    const pathString = s.substr(1)
    const pathComponents = pathString.split('.')

    let value = context
    for (const component of pathComponents) {
      const get = value.get ? value.get.bind(value) : (key) => value[key]
      
      let newValue = get(component)
      if (typeof newValue === 'undefined') {
        newValue = get(_.camelCase(component))
      }

      if (typeof newValue === 'undefined') {
        console.error('Error context:', context)
        throw new Error('Could interpolate ' + s)
      }

      value = newValue
    }

    return value
  }

  return templateString(s, context)
}

export function templateString(string, context) {
  if (!context) {
      // Nothing to do if no variables object was provided
      return string
  }

  return string.replace(/\$\{([^}]+)}/g, function (all, name) {
      let value = objectPath.get(
        context, name
      )
      if (typeof value === 'undefined') {
        value = objectPath.get(
          context, name.split('.').map(_.camelCase).join('.')
        )
      }
      return typeof value !== 'undefined' ? value : all
  })
}
