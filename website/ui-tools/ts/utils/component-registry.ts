export default class ComponentRegistry {
  private components

  constructor(components) {
    this.components = components
    // console.log(structures)
  }

  find(identifier) {
    const [moduleName, componentName] = identifier.split(':')
    const module = this.components[moduleName]
    if (typeof module === 'function') {
      return module(componentName)
    } else if (module) {
      return module[componentName]
    } else {
      return undefined
    }
  }
}
