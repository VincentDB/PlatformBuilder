const _ = require('lodash')
import * as objectPath from 'object-path'

export default class StyleRegistry {
  private byType = {}
  private byName = {}
  private byNameOrType = {}

  constructor(definition) {
    definition.forEach(rule => {
      // convert dashes to camelCase for CSS properties
      rule.style = _(rule.style).map(
        (value, key) => [_.camelCase(key), value]
      ).fromPairs().valueOf()

      if (_.isString(rule.selector)) {
        this.byNameOrType[rule.selector] = rule
        return
      }
      const type = rule.selector.type
      const name = rule.selector.name || '_default'
      if (type) {
        this.byType[type] = this.byType[type] || {}
        this.byType[type][name] = rule
      }
      if (name !== '_default') {
        this.byName[name] = rule
      }
    })
  }

  findRule(componentDefinition) {
    const rulesForType = this.byType[componentDefinition.type]
    if (rulesForType) {
      const rule = componentDefinition.name
        ? rulesForType[componentDefinition.name]
        : rulesForType['_default']

      if (rule) {
        return rule
      }
    }

    const rule = (this.byName[componentDefinition.name] ||
      this.byNameOrType[componentDefinition.name] ||
      this.byNameOrType[componentDefinition.type])

    return rule || null
  }

  find(componentDefinition, stateProps) {
    const rule = this.findRule(componentDefinition)

    if (rule) {
      const style = [rule.style]
      if (rule.conditionals) {
        rule.conditionals.forEach(condition => {
          // console.log('condition for "%o": %o', rule.selector, condition)

          if (!condition.if) {
            console.error('condition without if detected for selector', rule.selector)
            return
          }
          if (condition.if.substr(0, 1) !== '$') {
            console.error('invalid if "%s" for selector %s', rule.if, rule.selector)
            return
          }
          
          const isActive = objectPath.get(
            stateProps, condition.if.substr(1)
          )

          if (isActive) {
            style.push(condition.style)
          }
        })
      }
      return style
    }
  }
}
