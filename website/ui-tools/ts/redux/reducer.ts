const _ = require('lodash')
import { combineReducers } from 'redux-immutable'

export default function createRootReducer({modules, routerReducer = null, logActions = false}) {
  const combinedReducers = combineReducers({
    ...(routerReducer ? {routing: routerReducer} : {}),
    ..._.mapValues(modules, (module, moduleName) => {
      if (!module.initialState) {
        throw new Error('Module has no initial state: ' + moduleName)
      }

      return (state = module.initialState, action : {type? : string} = {}) => {
        let [actionModule, actionName, reducerName] = action.type.split('/')
        if (actionModule !== moduleName) {
          return state
        }
        actionName = _.camelCase(actionName)
        reducerName = reducerName && _.camelCase(reducerName)

        // console.log(action.type, module, actionName)

        const reducer = reducerName
          ? module[actionName].reducer[reducerName]
          : module[actionName].reducer
        if (reducer) {
          return reducer(state, action)
        } else {
          return state
        }
      }
    }),
  })

  return (state, action) => {
    if (logActions) {
      console.log('Reducing Redux action:', action)
    }
    return combinedReducers(state, action)
  }
}
