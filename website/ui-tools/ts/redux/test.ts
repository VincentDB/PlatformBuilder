const _ = require('lodash')
import * as fs from 'fs'
import * as path from 'path'
import * as traverse from 'traverse'
import * as objectPath from 'object-path'
import * as yaml from 'js-yaml'
import { detailedDiff } from 'deep-object-diff'
import { expect } from 'chai'
import * as sinon from 'sinon'
import * as redux from 'redux'
import backendMiddleware from './middleware/backend'
import { createModules } from './modules'
import createRootReducer from './reducer'
import { createAction as createCompositeAction } from './modules/dynamic/action-types/composite'
import { collectSuites } from '../test';

export function testSuite({suite, moduleName, createBackend, createServices}) {
  const suiteHelpers = {
    backend: (suite['test-helpers'] && suite['test-helpers'].backend) || {},
    services: (suite['test-helpers'] && suite['test-helpers'].services) || {}
  }
  suite.tests.forEach((test, testIdx) => {
    const {moduleDefinitions, modules} = createModules({
      moduleDefinitions: {[moduleName] : _.omit(suite, 'tests')},
      moduleOverrides: {}
    })
    let backend, services
    if (test.backend === 'dummy') {
      backend = createBackend({backendType: 'dummy'})
      services = createServices(backend, 'dummy')
    } else {
      backend = constructAsyncSource({...suiteHelpers.backend, ...(test.backend || {})})
      services = constructAsyncSource({...suiteHelpers.services, ...(test.services || {})})
    }
    
    const middleware = [
      backendMiddleware(backend, services),
    ]
    const rootReducer = createRootReducer({modules, logActions: process.env.LOG_ACTIONS === 'true'})
    
    it(test.should || `test ${testIdx + 1}`, async () => {
      const store = redux.applyMiddleware(...middleware)(redux.createStore)(rootReducer)
      
      let hasStepTests = false
      const stepTests = []

      let prevModuleState
      const getModuleState = () => {
        return traverse(store.getState().toJS()[moduleName]).forEach(function() {
          if (_.isPlainObject(this.node)) {
            this.update(_.mapKeys(this.node, (val, key) => _.kebabCase(key)))
          }
        })
      }

      await new Promise((resolve, reject) => {
        store.dispatch(<any>createCompositeAction({
          moduleDefinitions,
          modules: modules,
          moduleDefinition: moduleDefinitions[moduleName],
          module: modules[moduleName],
          reducer: null,
          actionDefinition: {
            steps: test.steps
          },
          input: {
            afterStep: async ({stepDefinition, stepIndex}) => {
              hasStepTests = hasStepTests ||
                _.has(stepDefinition, 'result') ||
                _.has(stepDefinition, 'difference')
              
              if (!hasStepTests) {
                return
              }

              const currentModuleState = getModuleState()
              let difference
              if (prevModuleState) {
                difference = _.pickBy(
                  detailedDiff(prevModuleState, currentModuleState),
                  elem => !_.isEmpty(elem)
                )
              }
              prevModuleState = currentModuleState

              if (stepDefinition.result) {
                stepTests.push([currentModuleState, stepDefinition.result])
              } else if (stepDefinition.difference) {
                stepTests.push([difference, stepDefinition.difference])
              } else {
                console.log(`Found step test without expected result: test ${testIdx + 1}, step ${stepIndex + 1}`)
                console.log(`Current output of test is:`)
                if (stepIndex === 0) {
                  console.log(yaml.safeDump({result: currentModuleState}))
                } else {
                  console.log(yaml.safeDump({difference}))
                }
              }
            },
            done: () => {
              resolve()
            }
          }
        }))
      })

      if (hasStepTests) {
        for (const stepTest of stepTests) {
          if (stepTest) {
            expect(stepTest[0]).to.deep.equal(stepTest[1])
          }
        }
      } else {
        if (test.result) {
          expect(getModuleState()).to.deep.equal(test.result)
        } else {
          getModuleState()
          console.log(`Found test without expected result: test ${testIdx + 1}`)
          console.log(`Current output of test is:`)
          // console.log(require('util').inspect(moduleState, false, null))
          console.log(yaml.safeDump({result: getModuleState()}))
        }
      }
    })
  })
}

function constructAsyncSource(sourceDefinition) {
  if (!sourceDefinition) {
    return {}
  }

  const source = {}
  for (const path in sourceDefinition) {
    const methodDefinition = sourceDefinition[path]
    const callDefinitions = methodDefinition instanceof Array ? methodDefinition : [methodDefinition]
    
    // let method = sinon.stub().throws("Stub method called without configured returned value")
    // for (const callDefinition of callDefinitions) {
    //   if (callDefinition.params) {
    //     method = method.withArgs(callDefinition.params)
    //   }
    //   if (callDefinition['call-number']) {
    //     method = method.onCall(callDefinition['call-number'] - 1)
    //   }
    //   method = method.returns(new Promise((resolve, reject) => {
    //     resolve(callDefinition.result)
    //   }))
    // }

    let callCount = 0
    const method = async (params) => {
      callCount += 1

      let filteredCalls = _(callDefinitions).filter(definition => {
        if (definition.params && !_.isEqual(params, definition.params)) {
          return false
        }
        if (definition['call-number'] && definition['call-number'] !== callCount) {
          return false
        }
        return true
      }).map(definition => {
        const hasParams = !!definition.params
        const hasCallNumber = !!definition['call-number']
        const specificity = (+hasParams) + (+hasCallNumber)
        return {...definition, specificity}
      }).orderBy(['specificity'], ['desc']).valueOf()

      if (_.isEmpty(filteredCalls)) {
        throw new Error(
          'Stub method ' + path + ' called with unexpected params: ' + JSON.stringify(params)
        )
      }

      const result = filteredCalls[0].result
      if (process.env.LOG_HELPER_STUBS === 'true') {
        console.log(
          'Helper stub call', callCount,
          'with params ', params,
          ', result', result)
      }

      return result
    }

    const camelCasePath = path.split('.').map(_.camelCase).join('.')
    objectPath.set(source, camelCasePath, method)
  }

  return source
}

export function testAppState(stateDataDir, {prefix = '', createBackend, createServices}) {
  const suites = collectSuites(stateDataDir, {prefix})
  suites.forEach(({suite, info: {moduleName}}) => {
    // suite.type = info.moduleName

    const description = `state suite: ${moduleName}`

    describe(description, () => {
      testSuite({suite, moduleName, createBackend, createServices})
    })
  })

  // const suitePaths = fs.readdirSync(stateDataDir)
  
  // suitePaths
  //   // .slice(0, 1)
  //   .forEach(suiteFileName => {
  //     if (path.extname(suiteFileName) !== '.yaml') {
  //       return
  //     }
  
  //     const suitePath = path.resolve(stateDataDir, suiteFileName)
  //     const suite = yaml.safeLoad(fs.readFileSync(suitePath), {
  //       filename: suitePath
  //     })
  //     const moduleName = suiteFileName.split('.').slice(0, -1).join('.')
  //     if (!suite || !suite.tests) {
  //       console.log(`Could not load suite state:${moduleName}`)
  //       return
  //     }
  //     if (process.env.TEST_ONLY && process.env.TEST_ONLY !== `state:${moduleName}`) {
  //       return
  //     }
  
  //     describe(`state suite: ${moduleName}`, () => {
  //       testSuite({suite, moduleName})
  //     })
  //   })  
}
