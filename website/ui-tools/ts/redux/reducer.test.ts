// import * as fs from 'fs'
// import * as path from 'path'
// import * as yaml from 'js-yaml'
// import { testSuite } from './test';

// const suitePaths = fs.readdirSync(path.resolve(__dirname, '../../state-tests'))

// suitePaths
//   // .slice(0, 1)
//   .forEach(suiteFileName => {
//     if (path.extname(suiteFileName) !== '.yaml') {
//       return
//     }

//     const suitePath = path.resolve(__dirname, '../../state-tests', suiteFileName)
//     const suite = yaml.safeLoad(fs.readFileSync(suitePath), {
//       filename: suitePath
//     })
//     const moduleName = 'state:test:' + suiteFileName.split('.').slice(0, -1).join('.')
//     if (!suite || !suite.tests) {
//       console.log(`Could not load suite ${moduleName}`)
//       return
//     }
//     if (process.env.TEST_ONLY && process.env.TEST_ONLY !== moduleName) {
//       return
//     }

//     describe(`state suite: ${moduleName}`, () => {
//       testSuite({suite, moduleName})
//     })
//   })
