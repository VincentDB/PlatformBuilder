const _ = require('lodash')
import * as objectPath from 'object-path'
import { interpolate } from '../../../utils/string'

export const idGenerator = (() => {
  let id = 1
  return () => ++id
})()

export function getValueDefinition({
  moduleDefinition, path
}) : any {
  let valueDefinition = {type: 'object', shape: moduleDefinition.state}
  for (let key of (typeof path === 'string' ? path.split('.') : path)) {
    if (valueDefinition.type === 'object') {
      valueDefinition = valueDefinition.shape[key]
    } else if (valueDefinition.type === 'list') {
      valueDefinition = valueDefinition['children']
    } else {
      throw new Error(`Could not find value definition ${path} in module ${moduleDefinition.name}`)
    }
  }
  return valueDefinition
}

export function getValueType({valueDefinition}) {
  return typeof valueDefinition !== 'string'
    ? valueDefinition.type
    : valueDefinition
}

export function findInObject(obj, path, context = {}) {
  if (typeof path === 'string') {
    return interpolate('$' + path, obj)
  }
  for (let key of path) {
    const get = obj.get ? obj.get.bind(obj) : (key) => obj[key]

    if (typeof key === 'string') {
      obj = get(interpolate(_.camelCase(key), context))
    } else {
      const selector = _.mapValues(key, part => interpolate(part, context))
      // console.log('s', selector)
      obj = _.find(obj.toJS ? obj.toJS() : obj, selector)
    }
  }
  return obj
}
