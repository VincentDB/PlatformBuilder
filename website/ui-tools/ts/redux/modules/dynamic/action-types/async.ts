const _ = require('lodash')
import { interpolate } from '../../../../utils/string'

export function createAction({
  moduleDefinition, actionDefinition, actionName, input, module, reducer
}) {
  const actionCreator = (dispatch, getState) => {
    dispatch({
      promise: (helpers) => {
        const source = interpolate(actionDefinition.source, helpers)
        let params
        try {
          params = _.mapValues(actionDefinition.params || [], param => {
            return interpolate(param, {
              globalState: getState(),
              state: getState().get(moduleDefinition.name),
              input,
              ...helpers
            })
          })
        } catch(e) {
          console.error(`Error while constructing input for action ${moduleDefinition.name}:${actionName}`)
          throw e
        }
        // console.log(params, source[_.camelCase(actionDefinition.method)](params))
        return source[_.camelCase(actionDefinition.method)](params)
      },
      done: ({result}) => {
        const {result: resultDefinition} = actionDefinition
        if (!resultDefinition) {
          return
        }

        const stepInput = _(_.omit(resultDefinition, 'action'))
          .mapKeys((val, key) => _.camelCase(key))
          .mapValues((val, key) => {
            return interpolate(val, {input, result})
          })
          .valueOf()

        const resultActionName = _.camelCase(resultDefinition.action)
        const action = module[resultActionName]
        if (!action) {
          throw new Error(
            'Async handler ' + actionName +
            ' could not find result handler action ' + resultDefinition.action)
        }
        dispatch(action.action(stepInput))

        if (input.done) {
          input.done()
        }
      },
      types: [
        reducer.request,
        reducer.success,
        reducer.fail
      ]
    })
  }
  return actionCreator
}

export function createReducers({
  moduleDefinition, actionDefinition, actionName
}) {
  if (!actionDefinition.progress) {
    return {
      request: state => state,
      success: state => state,
      fail: state => state,
    }
  }

  return {
    request: (state, action) => {
      return state.setIn(
        actionDefinition.progress.split('.').map(_.camelCase),
        'running'
      )
    },
    success: (state, action) => {
      return state.setIn(
        actionDefinition.progress.split('.').map(_.camelCase),
        'done'
      )
    },
    fail: (state, action) => {
      return state.setIn(
        actionDefinition.progress.split('.').map(_.camelCase),
        'error'
      )
    }
  }
}
