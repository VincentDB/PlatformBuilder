const _ = require('lodash')
import { interpolate } from '../../../../utils/string'
import * as jsonLogic from '../../../../utils/logic'
export { createReducers } from './async'

export function createAction(
  {moduleDefinition, moduleDefinitions, actionDefinition, input, modules, module, reducer = null} :
  {moduleDefinition, moduleDefinitions, actionDefinition, input, modules, module, reducer?}
) {
  return (dispatch, getState) => {
    const condition = actionDefinition.condition
    if (condition) {
      const context = {
        globalState: getState && getState(),
        state: getState && getState().get(moduleDefinition.name),
        input
      }

      let conditionFulfilled = false
      if (typeof condition === 'string') {
        conditionFulfilled = interpolate(condition, context)
      } else {
        conditionFulfilled = jsonLogic.apply(condition, context)
      }
      if (!conditionFulfilled) {
        return
      }
    }

    const makeStepInput = (stepDefinition) => {
      return _(_.omit(stepDefinition, 'action'))
        .mapKeys((val, key) => _.camelCase(key))
        .mapValues((val, key) => {
          return interpolate(val, {input})
        })
        .valueOf()
    }

    const makeStepPromise = (stepDefinition) =>
      new Promise((resolve, reject) => {
        // console.log('executing action', stepDefinition.action)
        const stepInput = makeStepInput(stepDefinition)
        let [moduleName, actionName] = stepDefinition.action.split(':')
        let action
        if (actionName) {
          actionName = _.camelCase(actionName)
          action = modules[moduleName][actionName].action
        } else {
          actionName = _.camelCase(moduleName)
          moduleName = moduleDefinition.name
          action = module[actionName].action
        }
        // console.log(actionName, stepInput)
        const actionDefinition = moduleDefinitions[moduleName].actions[_.kebabCase(actionName)]
        if (['async', 'composite'].indexOf(actionDefinition.type) >= 0) {
          dispatch(action({...stepInput, done: resolve}))
        } else {
          dispatch(action(stepInput))
          resolve()
        }
      })

    if (reducer && reducer.request) {
      dispatch({type: reducer.request})
    }

    let promise = Promise.resolve()
    actionDefinition.steps.forEach((stepDefinition, stepIndex) => {
      let newPromise
      if (stepDefinition.parallel) {
        newPromise = () => Promise.all(stepDefinition.parallel.map(
          childStepDefinition => makeStepPromise(childStepDefinition)
        ))
      } else {
        newPromise = () => makeStepPromise(stepDefinition)
      }
      promise = promise.then(newPromise)
      
      if (input.afterStep) {
        promise = promise.then(() => input.afterStep({stepDefinition, stepIndex}))
      }
    })

    promise.then(() => {
      if (reducer && reducer.success) {
        dispatch({type: reducer.success})
      }
      if (input.done) {
        input.done()
      }
    }).catch((e) => {
      console.error(e)
      if (reducer && reducer.fail) {
        dispatch({type: reducer.fail})
      }
    })
  }
}
