const _ = require('lodash')
import * as Immutable from 'immutable'
import * as jsonLogic from '../../../../utils/logic'
import { interpolate } from '../../../../utils/string'
import {
  findInObject, getValueDefinition, getValueType, idGenerator
} from '../utils'

export function createAction({
  moduleDefinition, actionDefinition, actionName, input, reducer
}) {
  return (dispatch, getState) => {
    // console.log('creating new item', input)
    // console.log('new item', createNewListItem({
    //   moduleDefinition, actionDefinition, input, getState
    // }))
    dispatch({
      type: reducer,
      value: createNewListItem({
        moduleDefinition, actionDefinition, input, getState
      }),
      input
    })
  }
}

export function createNewListItem({
  moduleDefinition, actionDefinition, input, getState
}) {
  if (typeof actionDefinition.value === 'string') {
    return interpolate(actionDefinition.value, {input})
  }

  const valueDefinition = getValueDefinition({
    moduleDefinition, path: actionDefinition.state
  }).children
  // console.log(valueDefinition)
  const valueType = getValueType({valueDefinition})
  if (valueType === 'object') {
    return _.mapValues(valueDefinition.shape, (childDefinition, key) => {
      const childType = getValueType({valueDefinition: childDefinition})
      if (childType === 'id') {
        return idGenerator()
      }
      const value = actionDefinition.value[key]
      // console.log(value)
      if (typeof value === 'string') {
        return interpolate(value, {input})
      }
      if (typeof value !== 'undefined') {
        return jsonLogic.apply(value, {
          state: getState && getState().toJS()[moduleDefinition.name],
          input
        })
      }
      if (typeof childDefinition.initial !== 'undefined') {
        return childDefinition.initial
      }
    })
  } else {
    return interpolate(actionDefinition.value, {input})
  }
}

export function createReducers({
  moduleDefinition, actionDefinition, actionName
}) {
  return (state, action) => {
    const jsState = state.toJS()
    const jsList = findInObject(jsState, actionDefinition.state)
    jsList.push(action.value)
    return Immutable.fromJS(jsState)
  }
}