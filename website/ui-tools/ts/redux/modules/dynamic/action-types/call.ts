const _ = require('lodash')
import { interpolate } from '../../../../utils/string'

export function createAction({
  moduleDefinition, actionDefinition, actionName, input, module, reducer
}) {
  const actionCreator = (dispatch, getState, helpers) => {
    const source = interpolate(actionDefinition.source, helpers)
    let params
    try {
      params = _.mapValues(actionDefinition.params || [], param => {
        return interpolate(param, {
          globalState: getState(),
          state: getState().get(moduleDefinition.name),
          input,
          ...helpers
        })
      })
    } catch(e) {
      console.error(`Error while constructing params for action ${moduleDefinition.name}:${actionName}`)
      throw e
    }
    // console.log(params, source[_.camelCase(actionDefinition.method)](params))
    const result = source[_.camelCase(actionDefinition.method)](params)
    const {result: resultDefinition} = actionDefinition
    if (!resultDefinition) {
      return
    }

    const stepInput = _(_.omit(resultDefinition, 'action'))
      .mapKeys((val, key) => _.camelCase(key))
      .mapValues((val, key) => {
        return interpolate(val, {input, result})
      })
      .valueOf()

    const resultActionName = _.camelCase(resultDefinition.action)
    const action = module[resultActionName]
    if (!action) {
      throw new Error(
        'Async handler ' + actionName +
        ' could not find result handler action ' + resultDefinition.action)
    }
    dispatch(action.action(stepInput))

    if (input.done) {
      input.done()
    }
  }
  return actionCreator
}

export function createReducers({
  moduleDefinition, actionDefinition, actionName
}) {
  return state => state
}
