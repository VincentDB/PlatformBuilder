const _ = require('lodash')
import * as Immutable from 'immutable'
import * as jsonLogic from '../../../../utils/logic'
import { interpolate } from '../../../../utils/string'
import { findInObject } from '../utils'

export function createAction({
  moduleDefinition, actionDefinition, actionName, input, reducer
}) {
  return (dispatch, getState, {backend, services}) => {
    const value = actionDefinition.value || `$input.${actionDefinition.state}`
    const state = getState().get(moduleDefinition.name)
    const context = {input, state, backend, services, self: null}
    const self = findInObject(state, actionDefinition.state, context)
    context.self = self
    
    dispatch({
      type: reducer,
      value: typeof value === 'string'
        ? interpolate(value, context)
        : jsonLogic.apply(
            value, context
          ),
      self,
      input
    })
  }
}

export function createReducers({
  moduleDefinition, actionDefinition, actionName
}) {
  return (state, action) => {
    const jsState = state.toJS()
    const path = (typeof actionDefinition.state === 'string'
      ? actionDefinition.state.split('.')
      : actionDefinition.state).map(elem => _.isString(elem) ? _.camelCase(elem) : elem)
    const obj = findInObject(jsState, path.slice(0, -1), {input: action.input})
    obj[_.last(path)] = action.value
    return Immutable.fromJS(jsState)
  }
}
