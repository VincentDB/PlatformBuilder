const _ = require('lodash')
import * as Immutable from 'immutable'

export function makeDefaultStateValue({
  moduleDefinition, valueDefinition, root, path = []
}) {
  if (typeof valueDefinition.initial !== 'undefined') {
    return valueDefinition.initial
  }
  const type = valueDefinition.type || valueDefinition
  if (type === 'object') {
    return _(valueDefinition.children).map((childDefinition, key) => {
      return [_.camelCase(key), makeDefaultStateValue({
        moduleDefinition,
        valueDefinition: childDefinition,
        root,
        path: [...path, key]
      })]
    }).fromPairs().valueOf()
  } else if (type === 'list') {
    return valueDefinition.initial || []
  } else if (type === 'progress') {
    return 'pristine'
  }
  throw new Error(
    'could not determine initial state value for ' +
    `${moduleDefinition.name}:${path.join('.')}`
  )
}

export function makeDefaultState({moduleDefinition}) {
  const root = {}
  return Immutable.fromJS(makeDefaultStateValue({
    moduleDefinition,
    valueDefinition: {type: 'object', children: moduleDefinition.state},
    root
  }))
}
