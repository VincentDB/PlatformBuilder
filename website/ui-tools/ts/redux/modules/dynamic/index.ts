const _ = require('lodash')
import * as objectPath from 'object-path'
import { interpolate } from '../../../utils/string'
// import { getActionTypeName } from './utils'
import { makeDefaultState } from './default-state'

const ACTION_TYPES = {
  'async': require('./action-types/async'),
  'call': require('./action-types/call'),
  'setter': require('./action-types/setter'),
  'list-push': require('./action-types/list-push'),
  'composite': require('./action-types/composite'),
}

export function createObservers(moduleDefinition, module) {
  return _.map(moduleDefinition.observers || [], observerDefinition => {
    return ({store, ...helpers}) => {
      const handler = event => {
        const params = _.mapValues(_.omit(observerDefinition, ['action', 'source']) || {}, param => {
          return interpolate(params, helpers)
        })
        store.dispatch(module[_.camelCase(observerDefinition.action)].action(params))
      }
      
      const source = interpolate(observerDefinition.source, helpers)
      source.events.on(observerDefinition.event, handler)
    }
  })
}

export function createDynamicModule({
  moduleDefinition, moduleDefinitions, modules
}) {
  moduleDefinition = preprocessDynamicModule(moduleDefinition)

  const module = _(moduleDefinition.actions).mapValues((actionDefinition, actionName) => {
    const {type: actionType} = actionDefinition
    if (!ACTION_TYPES[actionType]) {
      throw new Error('Unknown action type: ' + actionType)
    }

    const action = function actionCreator({reducer, ...input}) {
      return ACTION_TYPES[actionType].createAction({
        moduleDefinition, actionDefinition, actionName,
        input: _.mapKeys(input, (value, key) => _.kebabCase(key)),
        reducer,
        module,
        modules,
        moduleDefinitions
      })
    }

    const reducerMaker = ACTION_TYPES[actionDefinition.type].createReducers
    if (!reducerMaker) {
      return
    }
    const reducer = reducerMaker({
      moduleDefinition,
      actionDefinition,
      actionName
    })

    return { action, reducer, async: actionType === 'async' }
  }).mapKeys((val, key) => _.camelCase(key)).valueOf()

  // console.log(module)

  module.initialState = makeDefaultState({
    moduleDefinition
  })
  module.observers = createObservers(moduleDefinition, module)

  return module
}

export function preprocessDynamicModule(moduleDefinition) {
  _.each(moduleDefinition.state, (childDefinition, key) => {
    if (childDefinition['with-setter']) {
      moduleDefinition.actions[`set-${key}`] = {
        type: 'setter',
        state: key
      }
    }
  })
  _.each(moduleDefinition.actions, (actionDefinition, key) => {
    if (['async', 'call'].indexOf(actionDefinition.type) >= 0) {
      if (!actionDefinition.method) {
        const split = actionDefinition.source.split('.')
        actionDefinition.source = split.slice(0, -1).join('.')
        actionDefinition.method = split.slice(-1).join('.')
      }
      if (key.indexOf('retrieve-') === 0 && !actionDefinition.result) {
        const stateKey = key.substr('retrieve-'.length)
        actionDefinition.result = {
          action: `set-${stateKey}`,
          [stateKey]: '$result'
        }
      }
    }

    if (['composite', 'async'].indexOf(actionDefinition.type) >= 0) {
      if (!actionDefinition.progress) {
        actionDefinition.progress = `${key}-progress`
      }
      if (!moduleDefinition.state[actionDefinition.progress]) {
        moduleDefinition.state[actionDefinition.progress] = 'progress'
      }
    }
  })

  return moduleDefinition
}
