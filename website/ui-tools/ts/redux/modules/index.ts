const _ = require('lodash')
import { createDynamicModule } from './dynamic'

export var moduleDefinitions = {}
export var modules = {}

type TestMap = {[key : string] : number}

export function createModules({moduleDefinitions = {}, moduleOverrides = {}} = {}) {
  const customModules = {
    // router: require('./router').default,
    ...moduleOverrides
  }

  const modules = {}
  _.forEach(moduleDefinitions, (moduleDefinition, name) => {
    moduleDefinition.name = name
    modules[name] = createDynamicModule({moduleDefinition, modules, moduleDefinitions})
  })
  _.forEach(customModules, (module, name) => {
    modules[name] = modules[name] || {}
    Object.assign(modules[name], module)
  })

  _.forEach(modules, (module, moduleName) => {
    _.forEach(_.omit(module, ['initialState', 'observers']), (action, actionName) => {
      const reducer = typeof action.reducer === 'function'
        ? `${moduleName}/${_.snakeCase(actionName).toUpperCase()}`
        : _.mapValues(action.reducer, (f, reducerName) => {
          return [
            moduleName,
            _.snakeCase(actionName).toUpperCase(),
            _.snakeCase(reducerName).toUpperCase(),
          ].join('/')
        })

      const actionCreator = action.action
      action.action = (input) => {
        return actionCreator({...input, reducer})
      }
    })
  })

  return {modules, moduleDefinitions}
}

export function initModules(params : {moduleDefinitions?, moduleOverrides?} = {}) {
  modules = createModules(params)
  moduleDefinitions = params.moduleDefinitions
  return {moduleDefinitions, modules}
}
