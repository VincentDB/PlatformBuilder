export default function backendMiddleware(backend, services) {
  return ({dispatch, getState}) => {
    return next => action => {
      if (typeof action === 'function') {
        return action(dispatch, getState, {
          backend, services,
          // getActionType: action.getActionType
        })
      }

      const { promise, types, done, ...rest } = action
      if (!promise) {
        return next(action)
      }

      const [REQUEST, SUCCESS, FAILURE] = types
      next({...rest, type: REQUEST})

      const actionPromise = promise({backend, services})
      actionPromise.then(
        (result) => {
          next({...rest, result, type: SUCCESS})
          if (done) {
            done({result})
          }
        },
        (error) => {
          console.error(error, error.stack)
          next({...rest, error, type: FAILURE})
        }
      ).catch(error => {
        console.error('MIDDLEWARE ERROR:', error,
                      ...(error.stack ? [error.stack] : []))
        next({...rest, error, type: FAILURE})
      })

      return actionPromise
    }
  }
}
