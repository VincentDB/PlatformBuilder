"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
// import { compileRule } from '../logic-compiler'
function componentTemplate({ structure }) {
    const [module, name] = structure.type.split(':');
    let component = `
import React from 'react'
import Radium from 'radium'
import { connect } from 'redux/utils'
`.trim();
    component += '\n' + collectModuleImports(structure);
    if (structure['component-styles']) {
        component += `\nimport styles from './styles'`;
    }
    component += '\n';
    if (structure.state || structure.actions) {
        component += `
@connect({
${([
            structure.state && `props: [${statePropsTemplate(structure.state)}],`,
            structure.actions && `actions: [${actionsTemplate(structure.actions)}]`
        ].filter(line => !!line)).join('\n')}
})\n`;
    }
    component += `export default class ${_.upperFirst(_.camelCase(name))} {\n`;
    if (structure.handlers && structure.handlers.init) {
        component += componentDidMountTemplate({ structure });
    }
    component += renderMethodTemplate({ structure });
    component += `}`;
    return component;
}
exports.default = componentTemplate;
function componentDidMountTemplate({ structure }) {
    return `componentDidMount() {
  ${innerHandlerTemplate(structure.handlers.init)}
}`;
}
exports.componentDidMountTemplate = componentDidMountTemplate;
function renderMethodTemplate({ structure }) {
    let styleAttr = getStyleAttr(structure);
    styleAttr = styleAttr ? ` style=${styleAttr}` : '';
    return `render() {
    return (\n<div${styleAttr}>
      ${(structure.children || []).map(child => componentChildTemplate({
        parent: structure, definition: child
    }))}
    </div>\n)
  }`;
}
exports.renderMethodTemplate = renderMethodTemplate;
function collectModuleImports(definition, modules = {}, top = true) {
    _.forEach(definition.children || [], child => {
        const [module, name, type] = definition.type.split(':');
        if (module !== 'html') {
            modules[module] = true;
        }
        collectModuleImports(child, modules, false);
    });
    if (top) {
        return _.map(modules, (val, module) => {
            return `import * as ${_.camelCase(module)} from 'components/${module}'`;
        }).join('\n');
    }
}
exports.collectModuleImports = collectModuleImports;
function componentChildTemplate({ definition, parent }) {
    // return `s\n${indented(['s\ns', 'b', 'c'], 1, '++')}\ns`
    const children = definition.content || definition.children;
    const childrenArray = !_.isArray(children) ? [children] : children;
    const renderedChildren = _.isPlainObject(children) ? childrenArray.map(child => {
        return componentChildTemplate({
            definition: child, parent
        });
    }) : null;
    const attrString = constructAttrString({ definition });
    //   const userName = definition.name
    //   const userNameBeginString = userName && longChildren ? ` {/* begin: ${userName} */}` : ''
    //   const userNameEndString = userName && longChildren ? ` {/* end: ${userName} */}` : ''
    const [module, name] = definition.type.split(':');
    let tagName;
    if (module !== 'html') {
        if (definition['is-component']) {
            tagName = `${_.camelCase(module)}.${_.upperFirst(_.camelCase(name))}`;
        }
        else {
            tagName = 'div';
        }
    }
    else {
        tagName = name;
    }
    let element;
    if (_.isString(children)) {
        element = `<${tagName}${attrString}>
${valueTemplate(definition.content, { jsxLiteral: true })}
</${tagName}>`;
    }
    else if (renderedChildren) {
        element = `<${tagName}${attrString}>
${renderedChildren.join('\n')}
</${tagName}>`;
    }
    else {
        element = `<${tagName}${attrString} />`;
    }
    if (definition.repeat) {
        element = `{${valueTemplate(definition.repeat.source)}.map(${definition.repeat.target} => ${element})}`;
    }
    return element;
}
exports.componentChildTemplate = componentChildTemplate;
function constructAttrString({ definition }) {
    const attrs = {};
    if (definition['is-component']) {
        const inheritProps = (props) => {
            for (const key in props) {
                attrs[key] = `{this.props.${_.camelCase(key)}}`;
            }
        };
        inheritProps(definition['state-inherit']);
        inheritProps(definition['input-inherit']);
    }
    _.each(definition.attrs || {}, (val, key) => {
        attrs[key] = valueTemplate(val, { jsxAttr: true });
    });
    _.each(definition.handlers || {}, (handler, event) => {
        if (event === 'init') {
            return;
        }
        attrs[_.camelCase('on-' + event)] = handlerTemplate(handler);
    });
    if (definition['style']) {
        attrs['style'] = getStyleAttr(definition);
    }
    let attrString = _.map(attrs, (value, key) => `${key}=${value}`).join(' ');
    if (attrString) {
        attrString = ' ' + attrString;
    }
    return attrString;
}
exports.constructAttrString = constructAttrString;
function getStyleAttr(definition) {
    let attr;
    if (definition['style']) {
        attr = `{styles.${_.camelCase(definition['style-name'])}`;
        if (definition['style-dynamic']) {
            attr += '(this.props)';
        }
        attr += '}';
    }
    return attr;
}
exports.getStyleAttr = getStyleAttr;
function valueTemplate(val, { getters = {}, jsxAttr = false, jsxLiteral = false } = {}) {
    if (val.substr(0, 1) === '$') {
        let parts = val.substr(1).replace(':', '.').split('.');
        const getter = getters[parts[0]];
        let valString;
        if (getter) {
            valString = getter(...parts.slice(1));
            if (!valString) {
                throw new Error(`getter for ${val} returned null`);
            }
        }
        else {
            valString = '';
            if (['state', 'input'].indexOf(parts[0]) >= 0) {
                parts = parts.slice(1);
                valString = `this.props.`;
            }
            valString += parts.map(part => _.camelCase(part)).join('.');
        }
        if (jsxAttr || jsxLiteral) {
            valString = `{${valString}}`;
        }
        return valString;
    }
    else if (typeof val !== 'string') {
        return `${JSON.stringify(val)}`;
    }
    else if (jsxAttr) {
        return JSON.stringify(val);
    }
    else {
        return val;
    }
}
exports.valueTemplate = valueTemplate;
function statePropsTemplate(state) {
    return (state || []).map(id => {
        const [module, name, type] = id.split(':');
        return `'${module}.${_.camelCase(name)}'`;
    }).join(', ');
}
exports.statePropsTemplate = statePropsTemplate;
function actionsTemplate(actions) {
    return (actions || []).map(id => {
        const [module, name, type] = id.split(':');
        return `'${module}:${_.camelCase(name)}'`;
    }).join(', ');
}
exports.actionsTemplate = actionsTemplate;
function handlerTemplate(definition) {
    return `{event => ${innerHandlerTemplate(definition)}}`;
}
exports.handlerTemplate = handlerTemplate;
function innerHandlerTemplate(definition) {
    const action = _.camelCase(definition.action);
    const params = _.omit(definition, 'action');
    let call = `this.props.${action}(`;
    if (!_.isEmpty(params)) {
        call += `{${_.map(params, (val, key) => {
            return `${key}: ${valueTemplate(val, { getters: { event: eventKey => {
                        if (eventKey === 'value') {
                            return 'event.target.value';
                        }
                    } } })}`;
        }).join('\n')}}`;
    }
    call += ')';
    return call;
}
exports.innerHandlerTemplate = innerHandlerTemplate;
//# sourceMappingURL=index.js.map