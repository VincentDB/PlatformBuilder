import * as _ from 'lodash'
import * as fs from 'fs'
import * as esformatter from 'esformatter'
esformatter.register(require('esformatter-jsx'))

const TEMPLATES = {
  component: require('./component'),
  styles: require('./styles'),
  routes: require('./routes'),
}

module.exports = _.mapValues(TEMPLATES, (module, name) => {
  return (...args) => {
    const rendered = module.default(...args)
    try {
      return esformatter.format(rendered, {
        // https://github.com/millermedeiros/esformatter/wiki/Plugins
        plugins: ['esformatter-quotes', 'esformatter-quote-props'],
        quotes: {
          type: 'single',
          avoidEscape: true
        },
        jsx: {
          formatJSX: true,
          // move each attribute to its own line 
          attrsOnSameLineAsTag: false,
          // if lower or equal than 3 attributes, they will be kept on a single line 
          maxAttrsOnTag: 3,
          // keep the first attribute in the same line as the tag 
          firstAttributeOnSameLine: true,
          // default true, if false jsxExpressions won't be recursively formatted 
          formatJSXExpressions: false,
          // default true, if false the JSXExpressions might span several lines 
          JSXExpressionsSingleLine: true,
          // do not align attributes with the first tag 
          alignWithFirstAttribute: false,
          // default to one space. Make it empty if you don't like spaces between JSXExpressionContainers 
          spaceInJSXExpressionContainers: ' ',
          // default false. if true <React.Something /> => <React.Something/> 
          removeSpaceBeforeClosingJSX: false,
          // default false. if true attributes on multiple lines will close the tag on a new line 
          closingTagOnNewLine: false,
          // possible values single or double. Leave it as empty string if you don't want to modify the attributes' quotes 
          JSXAttributeQuotes: 'double',
          htmlOptions: {
            // put here the options for js-beautify.html 
          }
        }
      })
    } catch (e) {
      fs.writeFileSync(`/tmp/unformatted.${name}.js`, rendered)
      console.log(rendered.split('\n')
        // .map((line, idx) => {
        //   return `${idx + 1}  ${line}`
        // })
        .join('\n')
      )
      throw e
    }
  }
})
