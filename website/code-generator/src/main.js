"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('source-map-support').install();
require('regenerator-runtime/runtime');
const fs = require("fs");
const path = require("path");
const yaml = require('js-yaml');
// import processStructure from './utils/structure-processor'
// import LogicRegistry from './utils/logic-registry'
// import StyleRegistry from './utils/style-registry'
// import ComponentRegistry from 'rapid-ui/js/utils/component-registry'
const structure_loader_1 = require("./utils/structure-loader");
const structure_preprocessor_1 = require("./utils/structure-preprocessor");
const templates = require("./templates");
function maybeMakeDirSync(dir) {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
}
exports.maybeMakeDirSync = maybeMakeDirSync;
function processStructure({ structure, componentsDir }) {
    const [module, name] = structure.type.split(':');
    // const component = components.find(componentDefinition.type)
    const moduleDir = path.join(componentsDir, module);
    const componentDir = path.join(moduleDir, name);
    maybeMakeDirSync(moduleDir);
    const isDir = structure['is-component'] && structure['component-styles'];
    if (isDir) {
        maybeMakeDirSync(componentDir);
    }
    if (structure['is-component']) {
        const componentPath = isDir
            ? path.join(componentDir, 'index.jsx')
            : path.join(componentsDir, `module.jsx`);
        fs.writeFileSync(componentPath, templates.component({
            structure,
        }));
    }
    if (structure['component-styles']) {
        fs.writeFileSync(path.join(componentDir, 'styles.js'), templates.styles({
            structure
        }));
    }
    // const directChildrenAndSelf = componentDefinition.getChildren({
    //   onlyDirect: true, andSelf: true
    // })
    // const hasStyles = _.some(directChildrenAndSelf, child => {
    //   return !!styles.findRule(child)
    // })
    // fs.writeFileSync(
    //   path.join(componentDir, 'index.jsx'),
    //   (<any>templates).component({
    //     styles,
    //     componentDefinition,
    //     components,
    //     logic
    //   })
    // )
    // if (hasStyles) {
    //   fs.writeFileSync(
    //     path.join(componentDir, 'styles.js'),
    //     (<any>templates).styles({
    //       componentDefinition,
    //       styles,
    //       components,
    //       logic,
    //     })
    //   )
    // }
    for (const child of structure.children || []) {
        processStructure({ structure: child, componentsDir });
    }
}
exports.processStructure = processStructure;
function main() {
    const projectDir = process.env.PROJECT_DIR;
    const reactDir = path.resolve(projectDir, 'ui-react');
    const dataDir = path.resolve(projectDir, 'ui-data');
    const outputDir = path.resolve(projectDir, 'ui-react-generated');
    const componentsDir = path.join(outputDir, 'src/js/components');
    maybeMakeDirSync(componentsDir);
    // const componentModules = require(path.join(reactDir, 'src/js/component-modules'))
    // const components = new ComponentRegistry(componentModules)
    // const logic = new LogicRegistry(yaml.load(fs.readFileSync('../../../../data/rendering-logic/app.yaml')))
    // const styles = new StyleRegistry(yaml.load(fs.readFileSync('../../../../data/style/app.yaml')))
    let structure = structure_loader_1.default(fs.readFileSync(path.join(dataDir, 'structure/app.yaml')));
    structure = JSON.parse(structure);
    structure = structure_preprocessor_1.default(structure);
    fs.writeFileSync('/tmp/pre.yaml', yaml.dump(structure));
    processStructure({
        structure,
        componentsDir,
    });
    fs.writeFileSync(path.join(outputDir, 'src/js/routes.jsx'), templates.styles({
        structure
    }));
}
exports.main = main;
if (require.main === module) {
    main();
}
//process.on('unhandledRejection', (reason, p) => {
//  console.log('Unhandled Rejection at: ', p, 'reason:', reason);
// application specific logging, throwing an error, or other logic here
//});
//# sourceMappingURL=main.js.map