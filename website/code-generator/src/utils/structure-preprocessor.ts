const _ = require('lodash')

export default function preprocessStructure(structure) {
  determineComponents(structure)
  collectComponentStyles(structure)
  determineInheritedState(structure)
  determineInheritedInput(structure)
  return structure
}

export function determineComponents(structure) {
  let directChild = true
  directChild = directChild && (!structure.state && !structure.actions && !structure.route)
  directChild = directChild && !structure['is-include']
  structure['is-component'] = !directChild

  doForAllChildren(structure, determineComponents)
}

export function collectComponentStyles(structure) {
  const styles = {}
  
  const { style } = structure
  if (style) {
    let name
    if (structure['is-component']) {
      name = 'container'
    } else {
      name = structure.name || structure.type.split(':')[1]
    }
    structure['style-name'] = name
    styles[name] = style

    structure['style-dynamic'] = !!style.conditionals || _.some(style, val =>
      _.isString(val) && val.indexOf('$') !== -1
    )
  }

  doForAllChildren(structure, child => {
    Object.assign(styles, collectComponentStyles(child))
  })

  if (structure['is-component']) {
    if (!_.isEmpty(styles)) {
      structure['component-styles'] = styles
    }
    return {}
  } else {
    return styles
  }
}

export function determineInheritedState(structure) {
  const isInherited = (child, needed) => {
    return !_.some(child.state || [], state => {
      const name = state.split(':')[1].split('.').slice(-1)[0]
      return name === needed
    })
  }

  const getInherited = val => {
    if (val.indexOf('$state') !== 0) {
      return
    }

    return val.split('.')[1]
  }

  determineInherited({
    structure,
    key: 'state-inherited',
    isInherited,
    getInherited
  })
}

export function determineInheritedInput(structure) {
  const isInherited = (child, needed) => {
    return !_.some(child.input || {}, (type, key) => {
      return needed === key
    })
  }

  const getInherited = val => {
    const matches = /\$parent.*\.input\.([^\.]+)/.exec(val)
    if (matches) {
      return matches[1]
    }
  }

  determineInherited({
    structure,
    key: 'input-inherited',
    isInherited,
    getInherited
  })
}

export function determineInherited({structure, key, isInherited, getInherited}) {
  const inherited = {}
  doForAllChildren(structure, child => {
    determineInherited({structure: child, key, isInherited, getInherited})
    if (child[key]) {
      Object.assign(inherited, _.omitBy(child[key], (val, key) => {
        return !isInherited(structure, key)
      }))
    }
  })

  const checkForInherited = val => {
    if (!_.isString(val)) {
      return
    }

    const needed = getInherited(val)
    if (!needed) {
      return
    }
    if (isInherited(structure, needed)) {
      inherited[needed] = true
    }
  }
  const checkObjectForInherited = obj => {
    if(!_.isPlainObject(obj)) {
      return
    }

    _.each(obj, (val, key) => {
      checkForInherited(val)
    })
  }

  checkForInherited(structure.content)
  checkForInherited(structure.repeat && structure.repeat.source)
  checkObjectForInherited(structure.handlers)
  checkObjectForInherited(structure.attrs)
  checkObjectForInherited(structure.style)

  if (!_.isEmpty(inherited)) {
    structure[key] = inherited
  }
}

export function doForChildren(children, f) {
  for (const child of children || []) {
    if (typeof child === 'string') {
      continue
    }
    f(child)
  }
}

export function doForAllChildren(structure, f) {
  doForChildren(structure.children, f)
  doForChildren(structure.content, f)
}
